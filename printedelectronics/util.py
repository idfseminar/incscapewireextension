"""
Utility functions needed by several Inkscape extensions
that help creating printable electronics.

@authors: Michael Hedderich & Lena Hegemann
@date: 2015/12/14
"""

from printedelectronics.pathhelper import Path

def createSensorGrid(sensorNumX, sensorNumY, sensorWidth, sensorHeight, padding, originX, originY):
    """
    Creates a grid of touch sensors.
    Returns a list of rectangular Path objects that are positioned in a grid
    fashion (sensorNumX * sensorNumY rects).
    Each rectangular path has the size (sensorWidth,sensorHeight).
    Between two sensors the defined padding is placed in x and y direction.
    
    @param sensorNumX: Number of sensors in x direction (horizontal) 
    @param sensorNumY: Number of sensors in y direction (vertical)
    @param sensorWidth: Width of each sensor in x direction
    @param sensorHeight: Height of each sensor in y direction
    @param padding: Padding between each sensor (both on x and y direction)
    @param originX: Moves the grid on the x axixs. The grid starts at (originX,originY).
    @param originY: Moves the grid on the y axixs. The grid starts at (originX,originY).
    """
    grid = []      
    for x in range(sensorNumX):
        for y in range(sensorNumY):
            xPos = x*(sensorWidth + padding) + originX
            yPos = y*(sensorHeight + padding) + originY
            rectPath = Path()
            rectPath.setNodes([(xPos,yPos),(xPos,yPos+sensorHeight),(xPos+sensorWidth,yPos+sensorHeight),(xPos+sensorWidth,yPos)])
            grid.append(rectPath)
    return grid
    
def createPaddingGrid(sensorNumX, sensorNumY, sensorWidth, sensorHeight, padding, originX, originY):
    """
    Creates the paddings that form a grid of touch sensors when subtracted
    from a filled rect area (sort of the inverse of createSensorGrid).
    
    Returns a list of rectangular Polygon objects that represent the paddings
    between the sensors. The paddings overlap at the corners.
    
    @param sensorNumX: Number of sensors in x direction (horizontal) 
    @param sensorNumY: Number of sensors in y direction (vertical)
    @param sensorWidth: Width of each sensor in x direction
    @param sensorHeight: Height of each sensor in y direction
    @param padding: Padding between each sensor (both on x and y direction)
    @param originX: Moves the grid on the x axixs. The grid starts at (originX,originY).
    @param originY: Moves the grid on the y axixs. The grid starts at (originX,originY).
    """
    paddings = []      
    for x in range(sensorNumX-1):
        for y in range(sensorNumY):
            xPos = sensorWidth + x*(sensorWidth + padding) + originX
            yPos = originY
            if y != 0:
                yPos += -padding + y*(sensorHeight + padding)
            paddingHeight = padding + sensorHeight
            if y != 0 and y != sensorNumY:
                paddingHeight += padding
            paddingPath = Path()
            paddingPath.setNodes([(xPos,yPos),(xPos,yPos+paddingHeight),(xPos+padding,yPos+paddingHeight),(xPos+padding,yPos)])
            paddings.append(paddingPath)
            
    for y in range(sensorNumY-1):
        for x in range(sensorNumX):
            yPos = sensorHeight + y*(sensorHeight + padding) + originY
            xPos = originX
            if x != 0:
                xPos += -padding + x*(sensorWidth + padding)
            paddingWidth = padding + sensorWidth
            if x != 0 and x != sensorNumX:
                paddingWidth += padding
            paddingPath = Path()
            paddingPath.setNodes([(xPos,yPos),(xPos + paddingWidth,yPos),(xPos+paddingWidth,yPos+padding),(xPos,yPos+padding)])
            paddings.append(paddingPath)
    return paddings
    
def cleanUpPolygons(polygons):
    """
    The Shapely methods on Polygons like intersection or union
    do not always return a Polygon. Instead they might also 
    return a MultiPolygon (that contains several Polygons) or
    an empty object.
    This method expects a list of Polygon/MultiPolygon/Empty 
    objects and returns a list of Polygon objects. The Polygon
    objects from the MultiPolygon are extracted and the empty ones
    removed.
    """
    cleanPolygons = []
    for polygon in polygons:       
        if hasattr(polygon,'exterior'): #assume Polygon
            cleanPolygons.append(polygon)
        elif hasattr(polygon, 'geoms'): #assume MultiPolygon, extract contained Polygons
            cleanPolygons.extend(cleanUpPolygons(polygon.geoms))
        else: # assume GEOMETRYCOLLECTION EMPTY
            pass
    return cleanPolygons
