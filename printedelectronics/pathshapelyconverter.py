from shapely.geometry.polygon import Polygon
from shapely.geometry.linestring import LineString
from shapely.geometry.polygon import LinearRing
from printedelectronics import util
from printedelectronics.pathhelper import Path


class Converter:
    """
    Converter that converts from the pathhelper.Path class to
    Shapely Polygon and vice versa. 
    
    @authors: Michael Hedderich & Lena Hegemann
    @date: 2015/12/14
    """

    @staticmethod
    def pathToPolygon(path):
        """
        Expects a pathhelper.Path object and returns
        a Polygon.
        """
        nodes = []
        for i in range(path.getNumNodes()):
            nodes.append((path.getNodeX(i), path.getNodeY(i)))
        return Polygon(nodes).buffer(0)

    @staticmethod
    def pathToLineString(path):
        """
        Expects a pathhelper.Path object and returns
        a Line String.
        """
        nodes = []
        for i in range(path.getNumNodes()):
            nodes.append((path.getNodeX(i), path.getNodeY(i)))
        return LineString(nodes)

    @staticmethod
    def pathToLinearRing(path):
        """
        Expects a pathhelper.Path object and returns
        a Polygon build out of a Linear Ring.
        """
        nodes = []
        for i in range(path.getNumNodes()):
            nodes.append((path.getNodeX(i), path.getNodeY(i)))
        return Polygon(LinearRing(nodes))

    @staticmethod
    def __polygonToPath(polygon):
        """
        Internal convertion of a Shapely Polygon
        into a Path object.
        """
        coords = polygon.exterior.coords.xy
        coordsX = coords[0]
        coordsY = coords[1]
        path = Path()
        for i in range(len(coordsX)):
            path.addNodeAtEnd(coordsX[i], coordsY[i])
        return path

    @staticmethod
    def polygonsToPath(polygons):
        """
        Converts a list of Shapely Polygons into a list of Path objects. 
        Some Shapely operations on Polygons (like intersection) might
        return MultiPolygons/Empty obejcts instead of Polygons. 
        This method uses util.cleanUpPolygons() to transform these
        objects into several Polygons/remove them before conversion.
        """
        cleanPolys = util.cleanUpPolygons(polygons)
        return [Converter.__polygonToPath(poly) for poly in cleanPolys]

