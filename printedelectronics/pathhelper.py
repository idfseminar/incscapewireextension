import simplepath
import math
import inkex
import scipy
from printedelectronics.objects import Bezier
#just for fun making further development easier
arange = scipy.arange
ar     = scipy.array


class Path:
    """
     This class represents a path of nodes connected by edges. A path can be
     open or closed (i.e. an edge connects the first and the last node).
     
     The first node in the path has the position 0.
     
     It uses Inkscape's simplepath script and offers a wrapper to the
     handling of the list of commands and parameters.
     
     @authors: Michael Hedderich & Lena Hegemann
     @date: 2015/12/14
    """

    START_NODE_SYMBOL = 'M'
    NORMAL_NODE_SYMBOL = 'L'
    CLOSED_PATH_SYMBOL = 'Z'

    @staticmethod
    def isValid(svgElement):
        """
        Checks if the d-attribute of the given svg element contains a valid path.
        Returns False if this is not the case.
        """
        str_tag = svgElement.tag.replace("{http://www.w3.org/2000/svg}", "")
        if str_tag == "ellipse":
            return "cx" in svgElement.keys() and "cy" in svgElement.keys() and "rx" in svgElement.keys() and \
                   "ry" in svgElement.keys()
        elif str_tag == "circle":
            return "cx" in svgElement.keys() and "cy" in svgElement.keys() and "r" in svgElement.keys()
        elif str_tag == "rect":
            return "x" in svgElement.keys() and "y" in svgElement.keys() and "width" in svgElement.keys() and \
                   "height" in svgElement.keys()
        elif str_tag == "polygon" or str_tag == "polyline":
            return "points" in svgElement.keys()
        elif str_tag == "path":
            return Path._isValid(simplepath.parsePath(svgElement.get('d')))
        else:
            return False

    @staticmethod
    def _isValid(parsedSvgPath):
        #path must start with M element
        if parsedSvgPath[0][0] != 'M':
            return False

        #path must only contain straight lines (L) in the middle
        for element in parsedSvgPath[1:-1]:
                if not(element[0] == 'L' or element[0] == 'C' or element[0] == 'Q'):
                    return False

        #path is either closed (Z) or ends with a straight line (L)
        if not(parsedSvgPath[-1][0] == 'Z' or parsedSvgPath[-1][0] == 'L' or parsedSvgPath[-1][0] == 'C'):
            return False

        #everything fine, return true
        return True

    def __init__(self, svgElement=None):
        """
        Accepts nearly all SVG shapes as input.
		if not a path object, it is converted.
		Paths: http://www.w3.org/TR/SVG/paths.html
		Basic Shapes: http://www.w3.org/TR/SVG/shapes.html
        """
        self.isClosed = False
        self.path = []
        if svgElement is not None:
            str_tag = svgElement.tag.replace("{http://www.w3.org/2000/svg}", "")
            '''
            Converts a circle/ellipse to a path object
            '''
            if str_tag == "ellipse" or str_tag == "circle":
                self.isClosed = True
                flt_cx = float(svgElement.get("cx"))
                flt_cy = float(svgElement.get("cy"))
                if str_tag == "circle":
                    flt_rx = flt_ry = float(svgElement.get("r"))
                else:
                    flt_rx = float(svgElement.get("rx"))
                    flt_ry = float(svgElement.get("ry"))

                self.path.append(['M', [flt_cx + flt_rx, flt_cy]])
                for int_angle in range(1, 360):
                    flt_radiant = math.radians(int_angle)
                    self.path.append(['L', [flt_cx + math.cos(flt_radiant) * flt_rx,
                                            flt_cy + math.sin(flt_radiant) * flt_ry]])
            elif str_tag == "rect":
                '''
                Converts a rect to a path object:
                each line of the rect is represented by a line command
                '''
                flt_x = float(svgElement.get("x"))
                flt_y = float(svgElement.get("y"))
                flt_width = float(svgElement.get("width"))
                flt_height = float(svgElement.get("height"))
                self.isClosed = True
                self.path.append(['M', [flt_x, flt_y]])
                self.path.append(['L', [flt_x + flt_width, flt_y]])
                self.path.append(['L', [flt_x + flt_width, flt_y + flt_height]])
                self.path.append(['L', [flt_x, flt_y + flt_height]])

            elif str_tag == "polygon" or str_tag == "polyline":
                '''
                Converts a polygon/polyline to a path object:
                point list is converted to a list of move and line commands
                '''
                self.isClosed = str_tag == "polygon"
                str_pointlist = svgElement.get("points")
                aStr_lexerList = list(simplepath.lexPath(str_pointlist))
                self.path.append(['M', [float(aStr_lexerList[0][0]), float(aStr_lexerList[1][0])]])
                for int_i in range(1, len(aStr_lexerList)/2):
                    self.path.append(['L', [float(aStr_lexerList[int_i * 2][0]),
                                            float(aStr_lexerList[int_i * 2 + 1][0])]])
            elif str_tag == "path":
                tmp = simplepath.parsePath(svgElement.get('d'))
                if not(Path._isValid(tmp)):
                    raise Exception, "Given svg path is invalid."
                if tmp[-1][0] == 'Z': #remove closed symbol from list and set property isClosed to true instead
                    tmp = tmp[:-1]
                    self.isClosed = True

                obj_currentPosition = [0, 0]
                for segment in tmp:
                    # inkex.errormsg('%s'%str(segment))
                    '''
                    Converts a cubic/quadratic bezier curves to line commands
                    '''
                    if segment[0] == 'C' or segment[0] == 'Q':
                        controlpoints = [obj_currentPosition]
                        for i in arange(0, len(segment[1]), 2):
                            point = [segment[1][i], segment[1][i+1]]
                            controlpoints.append(point)
                        self.curve = Bezier(controlpoints)
                        k = 0.02
                        for t in arange(0, 1, k):
                            self.path.append(['L', self.curve.point(t)])
                        self.path.append(['L', self.curve.point(1)])
                    else:
                        self.path.append(segment)
                    obj_currentPosition = [segment[1][-2], segment[1][-1]]

    def addNode(self,position,x,y):
        """
        Inserts a new node to the path at the
        given position with the given coordinates
        """
        if position > len(self.path):
            raise Exception, "Position of new node is to large (" + str(position) + "). Path contains only " + str(len(self.path)) + " elements."
        if position < 0:
            raise Exception, "Position of new node must not be negative (" + str(position) + ")"

        if len(self.path) == 0:
            self._addNodeInternal(0,x,y,Path.START_NODE_SYMBOL)
        elif position == 0:
            self.path[0][0] = Path.NORMAL_NODE_SYMBOL
            self._addNodeInternal(0,x,y,Path.START_NODE_SYMBOL)
        else:
            self._addNodeInternal(position,x,y,Path.NORMAL_NODE_SYMBOL)

    def addNodeAtEnd(self,x,y):
        """
        Adds a new node at the end with the given coordinates
        """
        self.addNode(len(self.path),x,y)

    def _addNodeInternal(self,position,x,y,nodeType):
        self.path.insert(position, [nodeType,[x,y]])

    def setNodes(self,nodes):
        """
        Sets the nodes of this path to the given list
        of [x,y] coordinates
        """
        self.path = []
        self.addNodes(nodes)

    def addNodes(self,nodes):
        """
        Adds the given list of [x,y] coordinates to this path
        """
        for node in nodes:
            self.addNodeAtEnd(node[0],node[1])

    def getControls(self):
        """
        Returns list of control points in case
        it's a bezier curve
        """
        return self.controls

    def getNodeXY(self,position):
        """
        Returns a list containing the x and y coordinate
        of the node at the given position
        """
        return self.path[position][1]

    def getNodeX(self, position):
        return self.getNodeXY(position)[0]

    def getNodeY(self, position):
        return self.getNodeXY(position)[1]

    def setNodeXY(self,position,x,y):
        """
        Sets the coordinates of the node at the given position
        """
        self.path[position][1] = [x,y]

    def __len__(self):
        return len(self.path)

    def getNumNodes(self):
        return len(self)

    def isClosed(self):
        """
        Returns true if the path is closed (i.e. an edge connects
        the first and the last node)
        """
        return self.isClosed

    def setIsClosed(self,isClosed):
        """
        Set to True if the path should be closed (i.e. an edge connects
        the first and the last node)
        """
        self.isClosed = isClosed

    def debug(self):
        """
        Returns the current content of the internal path list
        for debugging
        """
        return self.path

    def toSVG(self,svgPath):
        """
        Sets the d-attribute of the given svg element
        to the path of this object.
        The given svg element must be a path.
        """
        if self.isClosed:
            self.path.append([Path.CLOSED_PATH_SYMBOL,[]])
        svgPath.set('d', simplepath.formatPath(self.path))
