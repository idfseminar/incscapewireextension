#!/usr/bin/env python

import inkex, simplestyle
from printedelectronics import util
from printedelectronics.pathhelper import Path
from printedelectronics.pathshapelyconverter import Converter
from shapely.geometry.polygon import Polygon

class MaskMultitouchEffect(inkex.Effect):
    """
    Creates a multitouch sensor consisting
    of a grid of sensors. Additionaly
    applies the currently selected object as 
    a mask on the grid (all parts of the
    grid outside the selected object are removed).
    The resulting multitouch grid has the
    shape of the selected object.
    
    The mask must consist only of straight lines.
    Bezier curves are not supported and cause straight
    lines (because the underlying Path object does not 
    handle Bezier curves yet)
    
    @authors: Michael Hedderich & Lena Hegemann
    @date: 2015/12/14
    """

    def __init__(self):
        inkex.Effect.__init__(self)
        
        # Options
        self.OptionParser.add_option('-x', '--sensors_x', action = 'store',
          type = 'int', dest = 'sensors_x', default = 50,
          help = 'x dimension')

        self.OptionParser.add_option('-y', '--sensors_y', action = 'store',
          type = 'int', dest = 'sensors_y', default = 50,
          help = 'y dimension')

        self.OptionParser.add_option('-p', '--padding', action = 'store',
          type = 'int', dest = 'padding', default = 20,
          help = 'padding')
          
        #Tab is only used for design purposes, but Inkscape sends this as another parameter  
        self.OptionParser.add_option('--tab', action = 'store', type = 'string', dest = 'ignore')  

    def effect(self):
        if not len(self.selected) == 1:
            inkex.errormsg("You must select exactly 1 element.")
            return
        
        converter = Converter()
        
        svg = self.document.getroot()

        # Create a new layer.
        layer = inkex.etree.SubElement(svg, 'g')
        layer.set(inkex.addNS('label', 'inkscape'), 'SensorLayer')
        layer.set(inkex.addNS('groupmode', 'inkscape'), 'layer')

        
        # Users might create invalid masks that the Path object can not handle.
        if not Path.isValid(self.selected.values()[0]):
            inkex.errormsg("The selected mask is invalid and can not be handled by the Path object. A reason might be that it contains curved lines. Only straight lines are supported.")
            return
            
        # From Inkscape svg to Path
        pathMask = Path(self.selected.values()[0])
        # From Path to Shapely Polygon
        polyMask = converter.pathToPolygon(pathMask)
        
        # Users might create invalid masks that Shapely can not handle.
        if not polyMask.is_valid:
            inkex.errormsg("The selected mask is invalid and can not be handled by the Shapely library. A reason might be crossing lines.")
            return
        
        # Bounding box of the mask is upper size limit for grid
        minx, miny, maxx, maxy = polyMask.bounds
        maskWidth = maxx - minx
        maskHeight = maxy - miny
        
        sensorWidth = self.options.sensors_x
        sensorHeight = self.options.sensors_y
        padding = self.options.padding
        
        sensorNumX = int((maskWidth + padding)/(sensorWidth + padding)) + 1
        sensorNumY = int((maskHeight + padding)/(sensorHeight + padding)) + 1
        
        # Create grid
        pathGrid = util.createSensorGrid(sensorNumX, sensorNumY, sensorWidth, sensorHeight, padding, minx, miny)
        # Convert Path objects in Grid to Shapely Polygons
        polyGrid = [converter.pathToPolygon(path) for path in pathGrid]
        
        # Apply mask on each element of the grid
        pathMaskedGrid = []
        for poly in polyGrid:
            polyMasked = poly.intersection(polyMask)
            
            pathMasked = converter.polygonsToPath([polyMasked])
            if isinstance(pathMasked, list):#multiple polygons created e.g. sensor cut into parts by mask
                pathMaskedGrid.extend(pathMasked)
            elif len(pathMasked) != 0: #one polygon, not empty
                pathMaskedGrid.append(pathMasked)
        
        # Convert cut out Path objects into SVG paths and add to layer.        
        for rectMasked in pathMaskedGrid:
            style = {'stroke' : 'none', 'stroke-width': '1', 'fill': '#000000'}
            attribs = {'style': simplestyle.formatStyle(style)}
            svgRectPath = inkex.etree.Element(inkex.addNS('path','svg'),attribs)
            rectMasked.toSVG(svgRectPath)
            layer.append(svgRectPath)  

effect = MaskMultitouchEffect()
effect.affect()

