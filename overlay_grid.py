#!/usr/bin/env python

import inkex
import shapely
import math
import copy


class OverlayGrid:

    # if the spacing is very less, we cannot route to all the sensors
    spacing_lower_bound = 20


    def __generate_logical_grid(self):
        """
        Generates a x-y-logical grid filled with 1 (= empty cell) and 0 (= occupied cell)
        :return: 0-1-grid with information about obstacles
        """

        logic_grid = [[1 for y in range(self.vertical_cell_amount)] for x in range(self.horizontal_cell_amount)]

        # if there are more cells on the image than there is space set the last row/column to occupied
        if self.cell_width_minimum % self.svg_width > 0:
            for y in range(self.vertical_cell_amount):
                logic_grid[self.horizontal_cell_amount - 1][y] = 0  # we set the half-cell to occupied

        if self.cell_height_minimum % self.svg_height > 0:
            for x in range(self.horizontal_cell_amount):
                logic_grid[x][self.vertical_cell_amount - 1] = 0  # we set the half-cell to occupied

        # Generate array of shaply boxes which cover the whole image to lookup obstacles
        self.box_grid = [[shapely.geometry.box(x * self.cell_width_minimum,
                    y * self.cell_height_minimum,
                    (x + 1) * self.cell_width_minimum,
                    (y + 1) * self.cell_height_minimum).convex_hull for y in range(self.vertical_cell_amount)] for x in range(self.horizontal_cell_amount)]


        adv_start_list = []
        adv_dst_list = []

        for obstacle in self.start_list:
            buffered = obstacle[2].buffer(self.spacing)
            occ_grid = []
            for y in range(self.vertical_cell_amount):
                for x in range(self.horizontal_cell_amount):
                    if buffered.intersects(self.box_grid[x][y]):
                        logic_grid[x][y] = 0  # occupied
                        occ_grid.append((x, y))
            adv_element = (obstacle[0], obstacle[1], occ_grid)
            adv_start_list.append(adv_element)

        # end points
        for obstacle in self.dst_list:
            buffered = obstacle[2].buffer(self.spacing)
            occ_grid = []
            for y in range(self.vertical_cell_amount):
                for x in range(self.horizontal_cell_amount):
                    if buffered.intersects(self.box_grid[x][y]):
                        logic_grid[x][y] = 0  # occupied
                        occ_grid.append((x, y))
            adv_element = (obstacle[0], obstacle[1], occ_grid)
            adv_dst_list.append(adv_element)

        self.start_list = adv_start_list
        self.dst_list = adv_dst_list
        return logic_grid

    def __transform_coords(self, coord_tupel):
        """
        Transforms pixel coordinates into grid coordintes
        :param coord_tupel: (x, y) - value
        :return: transformed (x, y) - value
        """
        (x, y) = coord_tupel
        res_x = int(min(x / self.cell_width_minimum, self.horizontal_cell_amount - 1))
        res_y = int(min(y / self.cell_height_minimum, self.vertical_cell_amount - 1))
        return res_x, res_y

    def __transform_coord_list(self, coord_list):
        """
        Transforms a list with pixel coordinates into a list with grid coordintes
        :param coord_list: list with (x, y, ?) - value
        :return: transformed_list (x, y, ?) - value transformed
        """
        transformed_list = []
        while len(coord_list) > 0:
            elem = coord_list.pop(0)

            trans_x, trans_y = self.__transform_coords((elem[0], elem[1]))
            transformed_list.append((trans_x, trans_y, elem[2]))
        return transformed_list

    def __init__(self, obstacle_list, in_start_list, in_dest_list, svg_width, svg_height, wire_size, spacing, cell_detail_factor):

        self.wire_size = wire_size

        # Sensor Spacing
        if spacing < self.spacing_lower_bound:
            inkex.errormsg('It may not be possible to connect to all sensors with this spacing. Choose a higher value')
        self.spacing = spacing

        # cell_width_minimum = (float) (wire_size + sensor_spacing)
        # cell_height_minimum = (float) (wire_size + sensor_spacing)

        self.cell_width_minimum = wire_size * cell_detail_factor
        self.cell_height_minimum = wire_size * cell_detail_factor

        self.svg_width = svg_width
        self.svg_height = svg_height

        # if the last cell would not fit on the page completely, we just add one more.
        # These half-cells are only used for points of direct wire connection
        self.horizontal_cell_amount = int(math.ceil(self.svg_width / self.cell_width_minimum))
        self.vertical_cell_amount = int(math.ceil(self.svg_height / self.cell_height_minimum))

        self.start_list = []
        self.dst_list = []
        self.wire_list = []

        self.start_list = self.__transform_coord_list(in_start_list)
        self.dst_list = self.__transform_coord_list(in_dest_list)

        self.logical_grid = self.__generate_logical_grid()

        self.obstacles = obstacle_list
        self.__load_obstacles_to_grid()

    def get_grid_manipulate_obstacles_id(self, pairID, reverse):
        """
        Returns a copy of the local grid. At the coordinates of the parameter list the value will be set to 1
        which indicates that the algorithm would prefer to connect through this cells. The cells will be
        checked with other obstacle coords.
        :param pairID: the index of the corresponding dest and starts in dest and start lists
        :param reverse: if true, the cells colliding with the pairID will be set to obstacles
        :return: a copy of the local grid with removed obstacles, or with the obstacles removed
        """
        result_grid = copy.deepcopy(self.logical_grid)
        #set the possible exception coordinates
        new_val = 1
        if reverse:
            new_val = 0 #we want to reset the obstacles

        ignoring_coords = self.start_list [pairID] [2] + self.dst_list [pairID] [2]

        for (x, y) in ignoring_coords:
            result_grid[x][y] = new_val
        if not reverse:
            #reset all other obstacle coordinates
            recheck_coords = []
            for i in range(0, len(self.dst_list)):
                if i != pairID:
                    recheck_coords = recheck_coords + self.dst_list [i] [2]
            for i in range(0, len(self.start_list)):
                if i != pairID:
                    recheck_coords = recheck_coords + self.start_list [i] [2]
            for wire in self.wire_list:
                recheck_coords = recheck_coords + wire [1]
            for (x, y) in recheck_coords:
                result_grid[x] [y] = 0
        return result_grid





    def get_grid_manipulate_obstacles(self, ignoring_coords, remove):
        """
        Returns a copy of the local grid. At the coordinates of the parameter list there will be no obstacles
        when remove is True. Otherwise obstacle boxes will be simulated.
        :param ignoring_coords: list of (x, y) coordinates where there will be no obstacle
        :param remove: if True than the obstacles at the positions will be removed. Otherwise they will be set
        :return: a copy of the local grid without some obstacles
        """
        result_grid = copy.deepcopy(self.logical_grid)
        if remove is True:
            new_val = 1
        else:
            new_val = 0

        for (x, y) in ignoring_coords:
            result_grid[x][y] = new_val

        return result_grid

    def add_rem_polygon(self, in_polygon, remove):
        """
        Returns a copy of the local grid. At the coordinates of the parameter list there will be no obstacles
        when remove is True. Otherwise obstacle boxes will be simulated.
        :param in_polygon: polygon, that will be added / removed from grid
        :param remove: if True than the obstacles at the positions will be removed. Otherwise they will be set
        :return: a copy of the local grid without some obstacles
        """
        # result_grid = copy.deepcopy(self.logical_grid)
        if remove is True:
            old_val = 0
            new_val = 1
        else:
            old_val = 1
            new_val = 0

        for y in range(self.vertical_cell_amount):
            for x in range(self.horizontal_cell_amount):
                if self.logical_grid[x][y] == old_val and in_polygon.intersects(self.box_grid[x][y]):
                    self.logical_grid[x][y] = new_val

    def __load_obstacles_to_grid(self):
        for poly in self.obstacles:
            buffered = poly.buffer(self.spacing)
            for y in range(self.vertical_cell_amount):
                for x in range(self.horizontal_cell_amount):
                    if buffered.intersects(self.box_grid[x][y]):
                        self.logical_grid[x][y] = 0



    def add_wire(self, line_string):
        """
        Saves the wire as shapely linestring
        :param line_string:
        :return:
        """
        buffered = line_string.buffer(self.wire_size/2 + self.spacing)
        occ_grid = []
        for y in range(self.vertical_cell_amount):
            for x in range(self.horizontal_cell_amount):
                if buffered.intersects(self.box_grid[x][y]):
                    self.logical_grid[x][y] = 0  # occupied
                    occ_grid.append((x, y))
        self.wire_list.append((line_string, occ_grid))