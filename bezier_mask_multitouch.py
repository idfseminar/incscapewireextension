#!/usr/bin/env python

import inkex, simplestyle
from printedelectronics import util
from printedelectronics.pathhelper import Path
from printedelectronics.pathshapelyconverter import Converter
from printedelectronics.objects import QuadraticBezier, CubicBezier
from shapely.geometry.polygon import Polygon
import scipy

#just for fun making further development easier
arange = scipy.arange

class BezierMaskMultitouchEffect(inkex.Effect):
    """
    Creates a multitouch sensor consisting
    of a grid of sensors. Additionaly
    applies the currently selected object as 
    a mask on the grid (all parts of the
    grid outside the selected object are removed).
    The resulting multitouch grid has the
    shape of the selected object.
    
    Bezier curves are supported
    
    @authors: Alice
    @date: 2016/01/...
    """

    def __init__(self):
        inkex.Effect.__init__(self)
        
        # Options
        self.OptionParser.add_option('-x', '--sensors_x', action = 'store',
          type = 'int', dest = 'sensors_x', default = 50,
          help = 'x dimension')

        self.OptionParser.add_option('-y', '--sensors_y', action = 'store',
          type = 'int', dest = 'sensors_y', default = 50,
          help = 'y dimension')

        self.OptionParser.add_option('-p', '--padding', action = 'store',
          type = 'int', dest = 'padding', default = 20,
          help = 'padding')
          
        self.OptionParser.add_option('-z', '--curve_type', action = 'store',
          type = 'string', dest = 'curve_type', default = 'path',
          help = 'curve type')
          
       # self.OptionParser.add_option('-q', '--curve_number', action = 'store',
        #  type = 'int', dest = 'curve_number', default = 2,
         # help = 'number of curves')
          
        #Tab is only used for design purposes, but Inkscape sends this as another parameter  
        self.OptionParser.add_option('--tab', action = 'store', type = 'string', dest = 'ignore')  

    def effect(self):
        if not len(self.selected) == 1:
            inkex.errormsg("You must select exactly 1 element.")
            return
        
        converter = Converter()
        
        svg = self.document.getroot()

        # Create a new layer.
        layer = inkex.etree.SubElement(svg, 'g')
        layer.set(inkex.addNS('label', 'inkscape'), 'SensorLayer')
        layer.set(inkex.addNS('groupmode', 'inkscape'), 'layer')

        sensorWidth = self.options.sensors_x
        sensorHeight = self.options.sensors_y
        padding = self.options.padding
        curve_type = self.options.curve_type
        #curve_number = self.options.curve_number
        
	    # Users might create invalid masks that the Path object can not handle.
        if (not Path.isValid(self.selected.values()[0])):
            inkex.errormsg("The selected mask is invalid and can not be handled by the Path object.")
            return
		# From Inkscape svg to Path
        pathMask = Path(self.selected.values()[0])
        polyMask = converter.pathToPolygon(pathMask)
		
        '''if (curve_type != 'path'):
            polyMask = converter.curveToBezier(pathMask)
        else:
            polyMask = converter.pathToPolygon(pathMask)'''
        
       # for point in list(polyMask.exterior.coords):
        #    self.draw_SVG_square((5,5), (point[0],point[1]), layer)
        
        #inkex.errormsg("valid polygon: "+str(polyMask.is_valid))
        #inkex.errormsg("object: %s"%polyMask.exterior.type)
        #inkex.errormsg("valid linearRing: "+str(polyMask.exterior.is_valid))
        
        # Bounding box of the mask is upper size limit for grid
        minx, miny, maxx, maxy = polyMask.bounds
        maskWidth = maxx - minx
        maskHeight = maxy - miny
        
        #inkex.errormsg("width: %d"%maskWidth+", height: %d"%maskHeight)
        
        sensorNumX = int((maskWidth + padding)/(sensorWidth + padding)) + 1
        sensorNumY = int((maskHeight + padding)/(sensorHeight + padding)) + 1
        
        # Create grid
        pathGrid = util.createSensorGrid(sensorNumX, sensorNumY, sensorWidth, sensorHeight, padding, minx, miny)
        # Convert Path objects in Grid to Shapely Polygons
        polyGrid = [converter.pathToPolygon(path) for path in pathGrid]
        
        # Apply mask on each element of the grid
        pathMaskedGrid = []
        for poly in polyGrid:
            polyMasked = poly.intersection(polyMask)
            
            pathMasked = converter.polygonsToPath([polyMasked])
            if isinstance(pathMasked, list):#multiple polygons created e.g. sensor cut into parts by mask
                pathMaskedGrid.extend(pathMasked)
            elif len(pathMasked) != 0: #one polygon, not empty
                pathMaskedGrid.append(pathMasked)
        
        # Convert cut out Path objects into SVG paths and add to layer.        
        for rectMasked in pathMaskedGrid:
            style = {'stroke' : 'none', 'stroke-width': '1', 'fill': '#000000'}
            attribs = {'style': simplestyle.formatStyle(style)}
            svgRectPath = inkex.etree.Element(inkex.addNS('path','svg'),attribs)
            rectMasked.toSVG(svgRectPath)
            layer.append(svgRectPath)
    
    
    def draw_SVG_square(self, (w,h), (x,y), parent):

		style = {   'stroke'        : 'none',
		            'stroke-width'  : '1',
		            'fill'          : '#00FF00'
		        }
		            
		attribs = {
		    'style'     : simplestyle.formatStyle(style),
		    'height'    : str(h),
		    'width'     : str(w),
		    'x'         : str(x),
		    'y'         : str(y)
		        }
		circ = inkex.etree.SubElement(parent, inkex.addNS('rect','svg'), attribs )
		
		
effect = BezierMaskMultitouchEffect()
effect.affect()

