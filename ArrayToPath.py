#!/usr/bin/env python
#-*- coding: utf-8 -*-

#generates squares with coordinates in "testArray", see below

import inkex, simplestyle

class ArrayToPath(inkex.Effect):
    def __init__(self):
        inkex.Effect.__init__(self)
        self.OptionParser.add_option('--some_name', action='store', type='string', dest='optionName',
                                     default='defaultvalue', help='Helper text for this option')
        self.OptionParser.add_option('--x_start', action='store', type='int', dest='x_start',
                                     default=5, help='Helper text for this option')
        self.OptionParser.add_option('--y_start', action='store', type='int', dest='y_start',
                                     default=8, help='Helper text for this option')
        self.OptionParser.add_option('--square_size', action='store', type='int', dest='square_size',
                                     default=1, help='Helper text for this option')

    def effect(self):
        x_start = self.options.x_start
        y_start = self.options.y_start
        startCoords = (x_start, y_start)
        squareSize = self.options.square_size

        createPath = (self.options.optionName == "path")
        self._main_function(startCoords, squareSize, createPath)
        svg = self.document.getroot

    def array_to_path(parent, testArray, startCoords, squareSize, createPath):

        if createPath:
            lineString = ""
            for coordinates in testArray:
                coordinates = align_coordinates(coordinates, startCoords, squareSize)
                if lineString == "":
                    lineString = 'M' #start with a move to the first coordinate
                else:
                    lineString = lineString + 'L' #move to the next line
                lineString = lineString + str(coordinates[0])+','+str(coordinates[1])
            draw_SVG_line(lineString, 1, "wire", parent)
        else:
            for coordinates in testArray:
                coordinates = align_coordinates(coordinates, startCoords, squareSize)
                draw_SVG_square(squareSize, squareSize, coordinates[0],coordinates[1],parent)

#grid coordinates
def align_coordinates(coords, startCoords,squareSize):
    return (startCoords[0] + coords[0] * squareSize, startCoords[1] + coords[1] * squareSize)



#SVG element generation routine
def draw_SVG_square(w, h, x, y, parent):

    style = {   'stroke'        : 'none',
                'stroke-width'  : '1',
                'fill'          : '#000000'
            }

    attribs = {
        'style'     : simplestyle.formatStyle(style),
        'height'    : str(h),
        'width'     : str(w),
        'x'         : str(x),
        'y'         : str(y)
            }
    circ = inkex.etree.SubElement(parent, inkex.addNS('rect','svg'), attribs )

#draw an SVG line segment between the given (raw) points
def draw_SVG_line(lineString, strokeWidth, name, parent):
    line_style   = { 'stroke'        : 'black',
                     'stroke-width': strokeWidth,
                     'fill': 'none'
                   }

    line_attribs = {'style' : simplestyle.formatStyle(line_style),
                    inkex.addNS('label','inkscape') : name,
                    'd' : lineString}

    line = inkex.etree.SubElement(parent, inkex.addNS('path','svg'), line_attribs )

if __name__ == '__main__':
    MyExtension = ArrayToPath()
    MyExtension.affect()