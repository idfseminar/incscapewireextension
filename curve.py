#!/usr/bin/env python

# These two lines are only needed if you don't put the script directly into
# the installation directory
import sys
sys.path.append('/usr/share/inkscape/extensions')

# We will use the inkex module with the predefined Effect base class.
import inkex, simplestyle
# The simplestyle module provides functions for style parsing.
from simplestyle import *
from printedelectronics.pathhelper import Path
from printedelectronics.pathshapelyconverter import Converter
from printedelectronics.bezier import Bezier
from printedelectronics.objects import QuadraticBezier, CubicBezier#, Path
import numpy as np
import math
import scipy


#just for fun making further development easier
pi     = scipy.pi
dot    = scipy.dot
sin    = scipy.sin
cos    = scipy.cos
ar     = scipy.array
rand   = scipy.rand
arange = scipy.arange
rad    = lambda ang: ang*pi/180                 #lovely lambda: degree to radian


def unit_vector(vector):
	""" Returns the unit vector of the vector.  """
	return vector / np.linalg.norm(vector)

def angle_between(v1, v2 = [10, 0]):
	""" Returns the angle in radians between vectors 'v1' and 'v2'"""
	v1_u = unit_vector(v1)
	v2_u = unit_vector(v2)
	angle = np.arccos(np.dot(v1_u, v2_u))
	if np.isnan(angle):
	    if (v1_u == v2_u).all():
	        return 0.0
	    else:
	        return np.pi
	return angle	

def Rotate(pts,cnt,ang):
    '''pts = {} Rotates points(nx2) about center cnt(2) by angle ang(1) in radian'''
    return dot(pts-cnt,ar([[cos(ang),sin(ang)],[-sin(ang),cos(ang)]]))+cnt


	
class PatternOnCurveEffect(inkex.Effect):

	def __init__(self):
		"""
		Constructor.
		Defines the "--what" option of a script.
		"""
		# Call the base class constructor.
		inkex.Effect.__init__(self)

		# Define string option "--what" with "-w" shortcut and default value "World".
		self.OptionParser.add_option('-n', '--number_sensors', action = 'store', type = 'int', 
			dest = 'number_sensors', default = 15, help = 'number of sensors')
		self.OptionParser.add_option('-s', '--sensor_size', action = 'store', type = 'int', 
			dest = 'sensor_size', default = 20, help = 'sensor size')
		self.OptionParser.add_option('-z', '--bezier_type', action = 'store', type = 'string', 
			dest = 'bezier_type', default = 'cubic', help = 'type of bezier curve')
		self.OptionParser.add_option('-k', '--sensor_distance', action = 'store', type = 'int', 
			dest = 'sensor_distance', default = 200, help = 'sensor distance')

		# Tab is only used for design purposes, but Inkscape sends this as another parameter  
		self.OptionParser.add_option('--tab', action = 'store', type = 'string', dest = 'ignore')
		
	'''for debugging only '''	
	def draw_SVG_square(self, (w,h), (x,y), parent):
		style = {   'stroke'        : 'none',
		            'stroke-width'  : '1',
		            'fill'          : '#00FF00'
		        }     
		attribs = {
		    'style'     : simplestyle.formatStyle(style),
		    'height'    : str(h),
		    'width'     : str(w),
		    'x'         : str(x),
		    'y'         : str(y)
		        }
		circ = inkex.etree.SubElement(parent, inkex.addNS('rect','svg'), attribs )
	
	def draw_triangle(self, tri, layer):
		style = {'stroke' : 'none', 'stroke-width': '1', 'fill': '#000000'}
		tri_attribs = {'style':formatStyle(style),
					'd':'M '+str(tri[0][0])+','	+str(tri[0][1])+	# p1
					' L '	+str(tri[1][0])+','	+str(tri[1][1])+	# p2
					' L '	+str(tri[2][0])+','	+str(tri[2][1])+	# p3
					' L '	+str(tri[0][0])+','	+str(tri[0][1])+' z'}	# to p1
		inkex.etree.SubElement(layer, inkex.addNS('path','svg'), tri_attribs)
			
	def effect(self):
		if not len(self.selected) == 1:
			inkex.errormsg("You must select exactly 1 element.")
			return
		
		# Get script's values
		number_sensors = self.options.number_sensors
		sensor_size = self.options.sensor_size
		sensor_distance = self.options.sensor_distance
		bezier_type = self.options.bezier_type
		
		# Get access to main SVG document element and get its dimensions.
		svg = self.document.getroot()
        
        # Create a new layer.
		layer = inkex.etree.SubElement(svg, 'g')
		layer.set(inkex.addNS('label', 'inkscape'), 'CurveLayer')
		layer.set(inkex.addNS('groupmode', 'inkscape'), 'layer')
		
		userInputObject = self.selected.values()[0]
		
		
		# from Inkscape svg to Path
		pathMask = Path(userInputObject)
		
		degree = pathMask.getNumNodes()
		if (bezier_type == 'cubic' and degree != 4) or (bezier_type == 'quatratic' and degree != 3):
			inkex.errormsg("The path is invalid. Either 3 or 4 points are allowed.")
		nodes = []
		points = []
		
		for i in range(0, degree):
			#Get the nodes from the control graph
			nodes.append(pathMask.getNodeXY(i))
			
		#Create Cubic/Quadratic Bezier with the control points
		if (bezier_type == 'cubic'):
			if (degree < 4):
				inkex.errormsg("The path is invalid. You need 4 points.")
			curve = CubicBezier(nodes[0], nodes[1], nodes[2], nodes[3])
		else:
			curve = QuadraticBezier(nodes[0], nodes[1], nodes[2])
		
		# triangle
		p1 = [0 , 0]
		p2 = [0 , sensor_size/2]
		p3 = [sensor_size , sensor_size/4]
		pts = ar([p3, p2, p1])
		#rotation point
		pr = [sensor_size/4, sensor_size/4]
		length = curve.length()
		offset = sensor_size * 0.25
		number_sensors = math.ceil(length / (sensor_size + offset))
		
		
		i = sensor_distance/1000.0
		tri0 = [[0, 0], [0, 1], [0.5, 0.5]]
		for j in np.arange(0, 1+i, i):
			
			#Calculation of slope and angle for rotation
			slope = curve.derivation(j)
			angle = angle_between(slope)
			if slope[1] < 0:
				angle = 2*pi - angle	
	
			point = curve.point(j)
			#DEBUGGING:draw squares without rotation to compare
			#self.draw_SVG_square((5,5), (point[0],point[1]), layer)
			points.append(point)
			ots = Rotate(pts,ar(pr),angle)
	
			#draw a triangle
			tri = [[point[0]+ots[0][0], point[1]+ots[0][1]] ,\
					[point[0]+ots[1][0], point[1]+ots[1][1]] ,\
					[point[0]+ots[2][0], point[1]+ots[2][1]] ]
			self.draw_triangle(tri, layer)
		
		
effect = PatternOnCurveEffect()
effect.affect()
		
