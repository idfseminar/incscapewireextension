#!/usr/bin/env python

# These two lines are only needed if you don't put the script directly into
# the installation directory
import sys
sys.path.append('/usr/share/inkscape/extensions')

# We will use the inkex module with the predefined Effect base class.
import inkex
# The simplestyle module provides functions for style parsing.
from simplestyle import *

from printedelectronics.pathhelper import Path

# currently for getting the value of pi
from math import *

class SimpletouchSensorEffect(inkex.Effect):
	"""
	Example Inkscape effect extension.
	Creates a new layer with a "Hello World!" text centered in the middle of the document.
	"""
	def __init__(self):
		"""
		Constructor.
		Defines the "--what" option of a script.
		"""
		# Call the base class constructor.
		inkex.Effect.__init__(self)

		# Define string option "--what" with "-w" shortcut and default value "World".
		self.OptionParser.add_option('-s', '--object_size', action = 'store',
		  type = 'int', dest = 'object_size', default = 100,
		  help = 'object size')

		self.OptionParser.add_option('-t', '--object_type', action = 'store',
		  type = 'string', dest = 'object_type', default = 'sensor_square',
		  help = 'selection type')

	def effect(self):
		"""
		Effect behaviour.
		Overrides base class' method and inserts "Hello World" text into SVG document.
		"""
		# Get script's values
		object_size = self.options.object_size
		type = self.options.object_type

		# Get access to main SVG document element and get its dimensions.
		svg = self.document.getroot()
		# or alternatively
		# svg = self.document.xpath('//svg:svg',namespaces=inkex.NSS)[0]

		# Again, there are two ways to get the attibutes:
		width  = self.unittouu(svg.get('width'))
		height = self.unittouu(svg.attrib['height'])

		# Create a new layer.
		layer = inkex.etree.SubElement(svg, 'g')
		layer.set(inkex.addNS('label', 'inkscape'), 'SensorLayer')
		layer.set(inkex.addNS('groupmode', 'inkscape'), 'layer')
		
		# Create rect element
		style = {'stroke' : 'none', 'stroke-width': '1', 'fill': '#000000'}
		attribs = {'height': str(object_size), 'width': str(object_size), 'x': '0', 'y': '0', 'style': formatStyle(style)}
		
		if type == 'sensor_square':
			inkex.etree.SubElement(layer, inkex.addNS('rect','svg'), attribs)
			
		elif type == 'sensor_circle':
			ell_attribs = {'style':formatStyle(style),
				inkex.addNS('cx','sodipodi')        :str(object_size/2),
				inkex.addNS('cy','sodipodi')        :str(object_size/2),
				inkex.addNS('rx','sodipodi')        :str(object_size/2),
				inkex.addNS('ry','sodipodi')        :str(object_size/2),
				inkex.addNS('start','sodipodi')     :str(0),
				inkex.addNS('end','sodipodi')       :str(2*pi),
				inkex.addNS('open','sodipodi')      :'true',    #all ellipse sectors we will draw are open
				inkex.addNS('type','sodipodi')      :'arc',
				'transform'                         :''
				}
			ell = inkex.etree.SubElement(layer, inkex.addNS('path','svg'), ell_attribs )
			
		elif type == 'sensor_triangle':
			tri_attribs = {'style':formatStyle(style)}
			path = Path()
			path.addNodeAtEnd(object_size/2,0)
			path.addNodeAtEnd(object_size,	object_size)
			path.addNodeAtEnd(0,			object_size)
			path.setIsClosed(True)
			path.toSVG(inkex.etree.SubElement(layer, inkex.addNS('path','svg'), tri_attribs ))
			
			
		elif type == 'sensor_star':
			tri_attribs = {'style':formatStyle(style)}
			path = Path()
			path.addNodeAtEnd(object_size*0.5,	0)
			path.addNodeAtEnd(object_size*0.81,	object_size)
			path.addNodeAtEnd(0,				object_size*0.38)
			path.addNodeAtEnd(object_size,		object_size*0.38)
			path.addNodeAtEnd(object_size*0.19,	object_size)
			path.setIsClosed(True)
			path.toSVG(inkex.etree.SubElement(layer, inkex.addNS('path','svg'), tri_attribs ))

# Create effect instance and apply it.
effect = SimpletouchSensorEffect()
effect.affect()
