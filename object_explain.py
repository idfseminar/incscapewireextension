#!/usr/bin/env python

import inkex
import simplepath
import re, math

class ObjectExplain(inkex.Effect):
    """
    Example that demonstrates the usage of the Path to/from
    shapely Polygon converter.

    Select two (!) overlapping (!) paths of straight lines. The
    effect will compute the union of the paths and replace the
    second element with the union.

    @authors: Michael Hedderich & Lena Hegemann
    @date: 2015/12/14
    """
    def __init__(self):
        inkex.Effect.__init__(self)

    def effect(self):
        # from Inkscape svg to Path
        obj_svgElement = self.selected.values()[0]
        str_tag = obj_svgElement.tag.replace("{http://www.w3.org/2000/svg}", "")
        str_output = ""
        str_output += "ObjectType: %s\n" % str_tag
        if str_tag == "ellipse":
            str_output += "Center X: %s\n" % obj_svgElement.get("cx")
            str_output += "Center Y: %s\n" % obj_svgElement.get("cy")
            str_output += "Radius X: %s\n" % obj_svgElement.get("rx")
            str_output += "Radius Y: %s\n" % obj_svgElement.get("ry")
            str_output += "https://www.w3.org/TR/SVG/shapes.html#EllipseElement"
        elif str_tag == "circle":
            str_output += "Center X: %s\n" % obj_svgElement.get("cx")
            str_output += "Center Y: %s\n" % obj_svgElement.get("cy")
            str_output += "Radius: %s\n" % obj_svgElement.get("r")
            str_output += "https://www.w3.org/TR/SVG/shapes.html#CircleElement"
        elif str_tag == "rect":
            str_output += "X: %s\n" % obj_svgElement.get("x")
            str_output += "Y: %s\n" % obj_svgElement.get("y")
            str_output += "Width: %s\n" % obj_svgElement.get("width")
            str_output += "Height: %s\n" % obj_svgElement.get("height")
            str_output += "https://www.w3.org/TR/SVG/shapes.html#RectElement"
        elif str_tag == "rect":
            str_output += "Start X: %s\n" % obj_svgElement.get("x1")
            str_output += "Start Y: %s\n" % obj_svgElement.get("y1")
            str_output += "Start X: %s\n" % obj_svgElement.get("x2")
            str_output += "Start Y: %s\n" % obj_svgElement.get("y2")
            str_output += "https://www.w3.org/TR/SVG/shapes.html#LineElement"
        elif str_tag == "polygon" or str_tag == "polyline":
            str_pointlist = obj_svgElement.get("points")
            aStr_lexerList = list(simplepath.lexPath(str_pointlist))
            aStr_pointlist = []
            for int_i in range(0, len(aStr_lexerList)/2):
                aStr_pointlist.append([float(aStr_lexerList[int_i * 2][0]), float(aStr_lexerList[int_i * 2 + 1][0])])
            for obj_command in aStr_pointlist:
                str_output += "%s\n" % str(obj_command)
            str_output += "https://www.w3.org/TR/SVG/shapes.html#PolylineElement"
        elif str_tag == "path":
            sp = simplepath.parsePath(self.selected.values()[0].get('d'))
            str_output += "Command-Count: %d\n" % len(sp)
            for obj_command in sp:
                str_output += "%s: %s\n" % (obj_command[0], str(obj_command[1]))
            str_output += "https://www.w3.org/TR/SVG/paths.html#PathData"
        else:
            str_output += "see https://www.w3.org/TR/SVG/"
        
        inkex.debug(str_output)
        # sp = simplepath.parsePath(self.selected.values()[0].get('d'))
        

effect = ObjectExplain()
effect.affect()

