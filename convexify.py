#!/usr/bin/env python

"""

"""
import sys

import inkex, simplestyle
from printedelectronics import util
from printedelectronics.pathhelper import Path
from printedelectronics.pathshapelyconverter import Converter
from shapely.geometry.polygon import Polygon
from shapely.geometry.linestring import LineString
from shapely.geometry import Point
import shapely.geometry.base
from shapely.ops import unary_union
from shapely.ops import polygonize

class Convexify(inkex.Effect):
    
	def __init__(self):
		inkex.Effect.__init__(self)
		
		self.OptionParser.add_option('-w', '--sensorsWidth', action = 'store',
		  type = 'int', dest = 'sensorsWidth', default = 20,
		  help = 'sensor width')
		  
		# Tab is only used for design purposes, but Inkscape sends this as another parameter  
		self.OptionParser.add_option('--tab', action = 'store', type = 'string', dest = 'ignore')
	
	def drawRect(self, x, y, size, color='#000000'):
		style = {'stroke' : str(color), 'stroke-width': '1', 'fill': 'none'}
		attribs = {'height': str(size), 'width': str(size), 'x': str(x), 'y': str(y), 'style': simplestyle.formatStyle(style)}
		inkex.etree.SubElement(layer, inkex.addNS('rect','svg'), attribs)
		
	def drawNumber(self, x, y, size, number, color='#000000'):
		style = {'stroke' : str(color), 'stroke-width': '1', 'fill': 'none'}
		for i in range(number):
			path = Path()
			path.addNodeAtEnd(x+(2*i), y)
			path.addNodeAtEnd(x+(2*i), y+size)
			attribs = {'style': simplestyle.formatStyle(style)}
			svgPath = inkex.etree.SubElement(layer, inkex.addNS('path','svg'), attribs)
			path.toSVG(svgPath)
		
	
	def calculateConcaveNodes(self, path):
		"""
		perfekte docu
		"""
		converter = Converter()
		poly = converter.pathToPolygon(path)
		
		polyConvexHull = poly.convex_hull
		pathConvexHull = converter.polygonsToPath([polyConvexHull])[0]
		
		# adapt node order of convex hull to node order of object
		pathConvexHullNormalized = Path()
		foundCorrespondingNode = False
		concaveNodeIndices = []
		for i in range(path.getNumNodes()):
			for j in range(pathConvexHull.getNumNodes()):
				if path.getNodeXY(i) == pathConvexHull.getNodeXY(j):
					pathConvexHullNormalized.addNodeAtEnd(pathConvexHull.getNodeX(j), pathConvexHull.getNodeY(j))
					foundCorrespondingNode = True
					break
			# if this node is missing in the convex hull, then it is a concave node
			if not foundCorrespondingNode:
				concaveNodeIndices.append(i)
			foundCorrespondingNode = False
		
		pathConvexHull = pathConvexHullNormalized
		
		return [concaveNodeIndices, pathConvexHull]
		
	def getCuttingLine(self, concaveNodeIndices, path):
		# TODO: loop over all concave nodes
		currentIndex = concaveNodeIndices[0]
		if path.getNumNodes() < 4:
			inkex.errormsg("cannot cut triangle, line or single dot")
			return #TODO return null or empty list or something like that
		
		#sourceX, sourceY = path.getNodeXY(concaveNodeIndices[currentIndex])
		source = Point(path.getNodeXY(currentIndex))
		linelengths = []
		
		minDistance = sys.float_info.max #1.7976931348623157e+308
		for i in range(path.getNumNodes()):
			target = Point(path.getNodeXY(i))
			if debug:
				self.drawNumber(target.x-15, target.y-15, 10, i+1)
			if (i == currentIndex) or ((i+1)%path.getNumNodes() == currentIndex) or ((i-1)%path.getNumNodes() == currentIndex):
				# ignore node itself and direct neighbours
				if debug:
					self.drawRect(target.x-5, target.y-5, 10, '#ff0000')
				continue
			#if i in concaveNodeIndices :
			#	# ignore other concave nodes
			#	self.drawRect(target.x-5, target.y-5, 10, '#ff0000')
			#	continue
			target = Point(path.getNodeXY(i))
			# TODO: check whether area(part1)/area(part2) --> 1 is a better heuristic
			distance = source.distance(target)
			linelengths.append(distance)
			if distance < minDistance:
				finalTarget = target
				if debug:
					self.drawRect(target.x-4, target.y-4, 10, '#ffa500')
				minDistance = distance
				#targetX = x
				#targetY = y
				
		# TODO: check whether line is actually inside the polygon
		if debug:
			inkex.errormsg(str(linelengths))
			inkex.errormsg("---")
			inkex.errormsg(str(minDistance))
			self.drawRect(finalTarget.x-3, finalTarget.y-3, 10, '#00ff00')
		return [source, finalTarget]
		
	
	def effect(self):
		if not len(self.selected) == 1:
			inkex.errormsg("You must select exactly 1 element.")
			return

		converter = Converter()

		svg = self.document.getroot()

		# Create a new layer.
		global layer
		layer = inkex.etree.SubElement(svg, 'g')
		layer.set(inkex.addNS('label', 'inkscape'), 'SensorLayer')
		layer.set(inkex.addNS('groupmode', 'inkscape'), 'layer')

		userInputObject = self.selected.values()[0]
		if not Path.isValid(userInputObject):
			inkex.errormsg("The selected object is invalid and can not be handled by the Path object. A reason might be that it contains curved lines. Only straight lines are supported.")
			return
		
		# from Inkscape svg to Path
		pathMask = Path(userInputObject)
		# from Path to Shapely Polygon
		polyMask = converter.pathToPolygon(pathMask)

		if not polyMask.is_valid:
			inkex.errormsg("The selected mask is invalid and can not be handled by the Shapely library. A reason might be crossing lines.")
			return
		
		# TODO: refactor code, e.g. into separate methods
		
		pathStyle = {'stroke' : '#000000', 'stroke-width': '1', 'fill': 'none'}
		
		global debug
		debug = False
		
		#inkex.errormsg(str(polyMask))
		
		concaveNodeIndices, pathConvexHull = self.calculateConcaveNodes(pathMask)
		polyConvexHull = converter.pathToPolygon(pathConvexHull)
		
		# if concave Nodes is empty, then there are no concave nodes 
		if not concaveNodeIndices:
			inkex.errormsg("The object is already convex")
			return
		
		# split path in two parts
		# find a good cutting line
		source, target = self.getCuttingLine(concaveNodeIndices, pathMask)
		
		
		
		line = LineString((source, target))
		exterior = polyMask.exterior
		segments = unary_union([exterior, line])
		poly_parts = list(polygonize(segments))
		
		
		pathMaskedGrid = (converter.polygonsToPath(poly_parts))
		
		style = {'stroke' : 'none', 'stroke-width': '1', 'fill': '#009900', 'fill-opacity': 0.5}
		for path in pathMaskedGrid:
			svgRectPath = inkex.etree.SubElement(layer, inkex.addNS('path','svg'), {'style': simplestyle.formatStyle(pathStyle)})
			path.toSVG(svgRectPath)
			
		if debug:
			pathLine = Path()
			pathLine.addNodeAtEnd(source.x, source.y)
			pathLine.addNodeAtEnd(target.x, target.y)
			
			pathStyle['stroke'] = '#00ff00'
			svgRectPath = inkex.etree.SubElement(layer, inkex.addNS('path','svg'), {'style': simplestyle.formatStyle(pathStyle)})
			pathLine.toSVG(svgRectPath)
			pathStyle['stroke'] = '#000000'
		
		
		#difference = polyConvexHull.difference(polyMask)
		
		#convexHullPathList = converter.polygonsToPath([polyConvexHull, difference])
		
		#pathStyle['stroke'] = '#ff0000'
		#svgRectPath = inkex.etree.SubElement(layer, inkex.addNS('path','svg'), {'style': simplestyle.formatStyle(pathStyle)})
		#convexHullPathList[0].toSVG(svgRectPath)
		
		#pathStyle['stroke'] = '#0000ff'
		#svgRectPath = inkex.etree.SubElement(layer, inkex.addNS('path','svg'), {'style': simplestyle.formatStyle(pathStyle)})
		#convexHullPathList[1].toSVG(svgRectPath)
	


effect = Convexify()
effect.affect()

