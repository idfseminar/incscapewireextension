import random
import sys
import copy
from heapq import *
from random import randint
import numpy
from munkres import Munkres, print_matrix

mx = 10; my = 10 # width and height of the maze
maze =numpy.array([[0 for x in range(mx)] for y in range(my)])

target=[]
destination=[]

def heuristic(a, b):
    return (b[0] - a[0]) ** 2 + (b[1] - a[1]) ** 2

def astar(array, start, goal):

    neighbors = [(0,1),(0,-1),(1,0),(-1,0),(1,1),(1,-1),(-1,1),(-1,-1)]

    close_set = set()
    came_from = {}
    gscore = {start:0}
    fscore = {start:heuristic(start, goal)}
    oheap = []

    heappush(oheap, (fscore[start], start))

    while oheap:

        current = heappop(oheap)[1]

        if current == goal:
            data = []
            while current in came_from:
                data.append(current)
                current = came_from[current]
            return data

        close_set.add(current)
        for i, j in neighbors:
            neighbor = current[0] + i, current[1] + j
            tentative_g_score = gscore[current] + heuristic(current, neighbor)
            if 0 <= neighbor[0] < array.shape[0]:
                if 0 <= neighbor[1] < array.shape[1]:
                    if array[neighbor[0]][neighbor[1]] == 1:
                        continue
                else:
                    # array bound y walls
                    continue
            else:
                # array bound x walls
                continue

            if neighbor in close_set and tentative_g_score >= gscore.get(neighbor, 0):
                continue

            if  tentative_g_score < gscore.get(neighbor, 0) or neighbor not in [i[1]for i in oheap]:
                came_from[neighbor] = current
                gscore[neighbor] = tentative_g_score
                fscore[neighbor] = tentative_g_score + heuristic(neighbor, goal)
                heappush(oheap, (fscore[neighbor], neighbor))

    return False


def GenerateRectangles(rectangles_count):
    #while not isSquare(rectangles_count):
    #rectangles_count+=1
    pinRectangleHeight=1
    pinRectangleWidth=rectangles_count*2

    pinRectangleX=mx//2-rectangles_count
    pinRectangleY=my-pinRectangleHeight-2
    #initialize maze 1-non passable sector, 0- passable sector
    for j in range(my):
        for i in range(mx):
            maze[j][i]=0

    #add connection pin points
    for x in range(rectangles_count):
            #maze[pinRectangleY][pinRectangleX+x*2]=1
            target.append((pinRectangleY,pinRectangleX+x*2))


    #add points representing rectangles center
    lower_y_bound=pinRectangleY-2;
    #y_offset=lower_y_bound/rectangles_count
   # x_offset=mx/rectangles_count
    for rectangle in range(rectangles_count):
        while True:
            y_rec=randint(2,lower_y_bound)
            x_rec=randint(2,mx-2)
            if not maze[y_rec][x_rec]==1:
                #maze[y_rec][x_rec]=1
                destination.append((y_rec,x_rec))
                break
    #find best connection pairs
    m = Munkres()
    matrix=[]
    for t in target:
        internal_target=[]
        for d in destination:
            result=astar(maze,t,d)
            internal_target.append(len(result))
        matrix.append(internal_target)
    indexes = m.compute(matrix)
    print_matrix(matrix, msg='Lowest cost through this matrix:')
    total = 0
    for row, column in indexes:
        value = matrix[row][column]
        total += value
        print '(%d, %d) -> %d' % (row, column, value)
    print 'total cost: %d' % total
    return indexes



assignment=GenerateRectangles(3)
value=3
for tar,dest in assignment:
    result=astar(maze,target[tar],destination[dest])
    for row,column in result:
        maze[row][column]=value
    value+=1
print maze



#    nmap2[row][column]=2
#print nmap2





























dx = [0, 1, 0, -1]; dy = [-1, 0, 1, 0] # directions to move in the maze
def GenerateMaze(cx, cy):
    maze[cy][cx] = 1
    while True:
        # find a new cell to add
        nlst = [] # list of available neighbors
        for i in range(4):
            nx = cx + dx[i]; ny = cy + dy[i]
            if nx >= 0 and nx < mx and ny >= 0 and ny < my:
                if maze[ny][nx] == 0:
                    # of occupied neighbors of the candidate cell must be 1
                    ctr = 0
                    for j in range(4):
                        ex = nx + dx[j]; ey = ny + dy[j]
                        if ex >= 0 and ex < mx and ey >= 0 and ey < my:
                            if maze[ey][ex] == 1: ctr += 1
                    if ctr == 1: nlst.append(i)
        # if 1 or more available neighbors then randomly select one and add
        if len(nlst) > 0:
            ir = nlst[random.randint(0, len(nlst) - 1)]
            cx += dx[ir]; cy += dy[ir]
            GenerateMaze(cx, cy)
        else: break



def solveMaze(grid, x_src, y_src, x_dst,y_dst,startValue):
    # The recursive algorithm. Starting at x and y, changes any adjacent
    # characters that match oldChar to newChar.

    if grid[y_src][x_src] == 0:
        print("Wrong starting point")
        print("StartValue= %d, Y=%d,X=%d"%(startValue,y_src,x_src))
        return False

    pictureWidth = len(grid)
    pictureHeight = len(grid[0])

    if x_src==x_dst and y_src==y_dst:
        print ("SOLVED")
        print (startValue)
        return startValue

    startValue+=1
    grid[y_src][x_src] = startValue

    # Recursive calls. Make a recursive call as long as we are not on the
    # boundary (which would cause an Index Error.)
    if x_src > 0 and grid[y_src][x_src-1]!=0 and grid[y_src][x_src-1]<3: # left
        return solveMaze(grid, x_src-1, y_src,x_dst,y_dst, startValue)


    if y_src > 0 and grid[y_src-1][x_src]!=0 and grid[y_src-1][x_src]<3: # up
        return solveMaze(grid, x_src, y_src-1,x_dst,y_dst, startValue)


    if x_src < pictureWidth-1 and grid[y_src][x_src+1]!=0 and grid[y_src][x_src+1]<3: # right
        return solveMaze(grid, x_src+1, y_src, x_dst,y_dst, startValue)


    if y_src < pictureHeight-1 and grid[y_src+1][x_src]!=0 and grid[y_src+1][x_src]<3: # down
        return solveMaze(grid, x_src, y_src+1,  x_dst,y_dst,startValue)







#GenerateMaze(0, 0)
# paint the maze
#for ky in range(imgy):
#    for kx in range(imgx):
#        m = maze[my * ky // imgy][mx * kx // imgx] * 255
#        pixels[kx, ky] = (m, m, m)
#image.save("RandomMaze_" + str(mx) + "x" + str(my) + ".png", "PNG")#

#result=solveMaze(maze,0,0,0,49,4)
#print(result)
#for ky in range(imgy):
#    for kx in range(imgx):
#        m = maze[my * ky // imgy][mx * kx // imgx]*100
#        pixels[kx, ky] = (m, m, m)
#image.save("RandomMaze_solution" + str(mx) + "x" + str(my) + ".png", "PNG")
#p=10