import random
import copy

from PIL import Image


def solveMaze(binary_grid, x_src, y_src, x_dst, y_dst, startValue):

    spacing = 10

    print("solving maze...")
    if (binary_grid[x_src][y_src] == 0) or (binary_grid[x_dst][y_dst] == 0):
        print("Wrong start or destination point")
        print("StartValue= %d, Y_src=%d,X_src=%d,Y_dst=%d,X_dst=%d "%(startValue,y_src,x_src,y_dst,x_dst))
        return False

    pictureHeight = len(binary_grid)
    pictureWidth = len(binary_grid[0])
    edge = [(x_src, y_src)]
    binary_grid[x_src][y_src] = startValue
    value = startValue
    solvable = False

    new_target = prepare_target(binary_grid, x_dst, y_dst, spacing)
    if new_target is False:
        return False

    (n_x_dst, n_y_dst) = new_target

    while edge:
        newedge = []
        value += 1
        for (x, y) in edge:
            if (x == n_x_dst) and (y == n_y_dst):
                solvable=True
                newedge = []
                break
            for (s, t) in ((x+1, y), (x-1, y), (x, y+1), (x, y-1)):
                if (0 <= s < pictureWidth) and (0 <= t < pictureHeight) and check_pix(s, t, binary_grid, spacing): # binary_grid[s][t] == 1):
                    binary_grid[s][t] = value
                    newedge.append((s, t))
        edge = newedge
    if not solvable:
        print("Maze is not solvable")
        colorize_maze(binary_grid, value)
        return False
    path = [(n_x_dst, n_y_dst)]
    while True:
        (x, y) = path[-1]
        if (x == x_src) and (y == y_src):
            break
        for (s, t) in ((x+1, y), (x-1, y), (x, y+1), (x, y-1)):
            if (0 <= s < pictureWidth) and (0 <= t < pictureHeight) and (binary_grid[s][t] == (binary_grid[x][y]-1)):
                path.append((s, t))
                break
    return path


def check_pix(x, y, binary_grid, spacing):
    pic_height = len(binary_grid)
    pic_width = len(binary_grid[0])

    result = True

    if binary_grid[x][y] != 1:
        result = False

    for i in range(1, spacing):
        for (s, t) in ((x+i, y), (x-i, y), (x, y+i), (x, y-i), (x+i, y+i), (x+i, y-i), (x-i, y+i), (x-i, y-i)):
            if (0 <= s < pic_width) and (0 <= t < pic_height) and (binary_grid[s][t] == 0):
                result = False
    return result


def prepare_target(binary_grid, x, y, spacing):
    for (s, t) in ((x+spacing, y), (x-spacing, y), (x, y+spacing), (x, y-spacing)):
        if check_pix(s, t, binary_grid, spacing) is True:
            return s, t
    print("Bad target point: Choose another target or reduce spacing")
    return False









def import_png(filesrc):
    """Imports an png image and converts it into a maze array with one and zero values
    :param filesrc: file source of the input png
    :returns 2D array filled with 1 for white pixels and 0 for black pixels"""
    print("importing " + str(filesrc) + "...")
    im = Image.open(filesrc)
    px = im.load()
    result_array = [[1 for x in range(im.width)] for x in range(im.height)]

    for i in range(0, im.width):
        for j in range(0, im.height):
            if px[i, j] != (255, 255, 255):
                result_array[i][j] = 0
    return result_array


def export_png(array):
    """
    Creates an image instance out of an color array.
    :param array: Color array with RGB triples
    :return: image instance with coresponding colors
    """
    dst_image = Image.new("RGB", (len(array), len(array[0])))
    dst_pixel = dst_image.load()
    for x in range(len(array)):
        for y in range(len(array[0])):
            dst_pixel[x, y] = array[x][y]
    return dst_image


def colorize_maze(input_array, max_value):
    """
    Saves an image out of an array filled with integer values. Each integer provides his own color in the final image
    :param input_array: Array with integer values
    :param max_value the maximal value in the integer array
    """
    array_width = len(input_array)
    array_height = len(input_array[0])

    # Initializing color array completly white
    color_array = [[(255, 255, 255) for x in range(array_width)] for y in range(array_height)]

    for x in range(len(input_array)):
        for y in range(len(input_array[0])):
            color_val = (input_array[x][y]) * (255 / max_value)
            color_array[x][y] = (0, int(color_val), int(color_val))
    colored_img = export_png(color_array)
    colored_img.save("ColorMaze.png", "PNG")


def maze_to_image(maze_array, dst_image):
    """ Generates an array with white and black pixels out of an maze array with zeros (= black) and ones (=white)
    :param maze_array: input array of maze data. Zeros and ones in array
    :param dst_image: image instance where the maze will be printed """
    dst_pixel_array = dst_image.load()
    img_width = dst_image.width
    img_height = dst_image.height

    maze_width = len(maze_array)
    maze_height = len(maze_array[0])

    for y in range(img_height):
        for x in range(img_width):
            color = maze_array[maze_width * x // maze_width][maze_height * y // maze_height] * 255
            dst_pixel_array[x, y] = (color, color, color)


def add_print_path(path_array, dst_image, color):
    """Adds a matching print path to an image with the wished color
    :param path_array: Array with all path pixel coordinates
    :param dst_image: image instance where the path will be added to
    :param color: RGB color of the image path. Example: RED = (255, 0, 0)
    """
    img_pix = dst_image.load()
    img_width = dst_image.width
    img_height = dst_image.height

    for x in range(img_width):
        for y in range(img_height):
            in_x = img_width * x // img_width
            in_y = img_height * y // img_height
            if (in_x, in_y) in path_array:
                img_pix[x, y] = color

input_maze = import_png("Unbenannt.png")
result_path = solveMaze(copy.deepcopy(input_maze), 0, 0, 327, 351, 2)

print(str(result_path))

if result_path is False:
    print("No wire connection possible")
else:
    print("MazeSolved")
    print("exporting result...")
    result_image = Image.new("RGB", (400, 400))

    maze_to_image(input_maze, result_image)
    add_print_path(result_path, result_image, (255, 0, 0))
    result_image.save("MazeSolution.png", "PNG")
    print("done")
