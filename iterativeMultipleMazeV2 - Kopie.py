import random
import sys
import copy
import math
from random import randint


def solveMaze(binary_grid, x_src, y_src, x_dst,y_dst):
    startValue=4
    #if (binary_grid[y_src][x_src] or binary_grid[y_dst][x_dst]) == 0:
     #   print("Wrong start or destination point")
      #  print("StartValue= %d, Y_src=%d,X_src=%d,Y_dst=%d,X_dst=%d "%(startValue,y_src,x_src,y_dst,x_dst))
       # return False

    pictureHeight = len(binary_grid)
    pictureWidth = len(binary_grid[0])
    edge = [(x_src,y_src)]
    binary_grid[y_src][x_src] = startValue
    value=startValue
    solvable=False
    while edge:
        newedge = []
        value+=1
        for (x, y) in edge:
            if (x==x_dst and y==y_dst):
                # print(value)
                solvable=True
                break
            for (s, t) in ((x+1, y), (x-1, y), (x, y+1), (x, y-1)):
                if 0<=s<pictureWidth and  0<=t<pictureHeight:
                    if binary_grid[t][s]==1: # or binary_grid[t][s]==2:
                        binary_grid[t][s] = value
                        newedge.append((s, t))
        edge = newedge
    if not solvable:
        # print("Maze is not solvable")
        return False
    path=[(x_dst,y_dst)]
    while True:
        (x,y) =path[len(path)-1]
        if (x==x_src and y==y_src):
            break
        for (s, t) in ((x+1, y), (x-1, y), (x, y+1), (x, y-1)):
            if 0<=s<pictureWidth and  0<=t<pictureHeight and binary_grid[t][s]==binary_grid[y][x]-1:
               path.append((s,t))
    return path


#GenerateMaze(0, 0)
'''
assignment=GenerateRectangles(5)
# paint the maze
for ky in range(imgy):
    for kx in range(imgx):
        m = maze[my * ky // imgy][mx * kx // imgx] * 255
        pixels[kx, ky] = (m, m, m)
image.save("RandomMaze_" + str(mx) + "x" + str(my) + ".png", "PNG")
print("Maze Generated")

for tar, dest in assignment:
    result=solveMaze(copy.deepcopy(maze),target[tar][0],target[tar][1],destination[dest][0],destination[dest][1])
    # paint Maze result if solvable
    if not result ==False:
        print("MazeSolved")
        result.reverse()
        for path_pair in result:
            maze[path_pair[1]][path_pair[0]]=0
        print( result )
        for ky in range(imgy):
            for kx in range(imgx):
                in_x=mx * kx // imgx
                in_y=my * ky // imgy
                if (in_x,in_y) in result:
                    pixels[kx, ky] = (255, 0, 0)
                else:
                    m = maze[in_y][in_x] * 255
                    pixels[kx, ky] = (m, m, m)
    image.save("RandomMazeSolution_" + str(mx) + "x" + str(my) + ".png", "PNG")

'''