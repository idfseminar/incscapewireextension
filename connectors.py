#!/usr/bin/env python

import inkex
import simplestyle
from inkex import debug


class ConnectorSpace:

    def add_croc_connector(self):
        """
        Adds crocodile connectors to the connector layer
        """
        # need num_sensors markers for connectors
        if self.settings.conn_pos1 == 'bottom' or self.settings.conn_pos1 == 'top':
            croc_h = 50
            croc_w = 20
            # if self.settings.conn_spread == 'compact':
            croc_pad = self.settings.conn_spacing
            # else:
            #    croc_pad = (self.svgwidth - ((self.num_sensors + 1) * croc_w)) / self.num_sensors
            style = {'fill': '#000000',
                     'stroke': 'none',
                     'stroke-width': '2'}
            if self.settings.conn_pos1 == 'bottom':
                y = self.svgheight - croc_h
            else:
                y = 0
            for i in range(self.num_sensors):
                x = (i+1) * croc_pad + (i * croc_w)
                attribs = {
                    'style': simplestyle.formatStyle(style),
                    'x': str(x),
                    'y': str(y),
                    'height': str(croc_h),
                    'width': str(croc_w)
                }
                crect = inkex.etree.Element(inkex.addNS('rect', 'svg'), attribs)
                self.layer.append(crect)

    def add_parallel_connector(self, num_conn):
        """
        both bottom/top aligns the pads in the center
        """
        if self.num_sensors > 20:
            inkex.errormsg('nr. Sensors > 20 NOT supported')
            return
        if num_conn == 20:
            wrap_at = 9
        else:
            wrap_at = 7
        if self.settings.conn_pos1 == 'bottom' or self.settings.conn_pos1 == 'top':
            conn10_h = 10
            conn10_pad = 5
            conn10_w = 4
            tconn_width = (self.num_sensors + 1) * (conn10_w + conn10_pad)

            mid_pt = self.svgwidth / 2
            x_offset = mid_pt - (tconn_width / 2)
            style = {'stroke': 'black',
                     'stroke-width': str(conn10_w)}
            if self.settings.conn_pos1 == 'bottom':
                y = self.svgheight - (0.1 * self.svgheight)
            else:
                y = 0.1 * self.svgheight
            for i in range(num_conn):
                if i > wrap_at:
                    x = ((i-wrap_at) * conn10_pad) + ((i-wrap_at-1) * conn10_w)
                    y_offset = 20
                else:
                    x = (i+1) * conn10_pad + (i * conn10_w)
                    y_offset = 0
                attribs = {
                    'style': simplestyle.formatStyle(style),
                    'x1': str(x_offset + x),
                    'y1': str(y + y_offset),
                    'x2': str(x_offset + x),
                    'y2': str(y + y_offset + conn10_h)
                }
                crect = inkex.etree.Element(inkex.addNS('line', 'svg'), attribs)
                self.layer.append(crect)

    def __init__(self, options, svg_width, svg_height, layer, sensor_amount):
        self.settings = options
        self.num_sensors = sensor_amount
        self.svgwidth = svg_width
        self.svgheight = svg_height
        self.layer = layer
