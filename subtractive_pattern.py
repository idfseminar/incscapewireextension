#!/usr/bin/env python

# These two lines are only needed if you don't put the script directly into
# the installation directory
import sys
sys.path.append('/usr/share/inkscape/extensions')

# We will use the inkex module with the predefined Effect base class.
import inkex, simplestyle
# The simplestyle module provides functions for style parsing.
from simplestyle import *
from printedelectronics.pathhelper import Path
from printedelectronics.pathshapelyconverter import Converter
from printedelectronics.objects import Bezier
import numpy as np
import math
import scipy

arange = scipy.arange


class PatternOnCurveEffect(inkex.Effect):

	def __init__(self):
		"""
		Constructor.
		Defines the "--what" option of a script.
		"""
		# Call the base class constructor.
		inkex.Effect.__init__(self)

		# Define string option "--what" with "-w" shortcut and default value "World".
		self.OptionParser.add_option('-k', '--sensor_distance', action = 'store', type = 'int', 
			dest = 'sensor_distance', default = 20, help = 'number of sensors')
		self.OptionParser.add_option('-t', '--gap', action = 'store', type = 'int', 
			dest = 'gap', default = 5, help = 'gap between sensors')
		self.OptionParser.add_option('-p', '--line_thickness', action = 'store', type = 'int', 
			dest = 'line_thickness', default = 30, help = 'thickness of sensor')
		# Tab is only used for design purposes, but Inkscape sends this as another parameter  
		self.OptionParser.add_option('--tab', action = 'store', type = 'string', dest = 'ignore')
		
	
	def draw_line(self, width, (x,y), (z,w), parent):
		'''
		draws the actual path of the sensor. Necessary because
		getting the points from a predrawn Bezier curve does not work yet.
		'''
		style = {   'stroke'        : '#000000',
			        'stroke-width'  : str(width),
			        'fill'          : '#000000'}
		attribs = {
			'style':formatStyle(style),
			'd':'M '+str(x)+','	+str(y)+	
			' L '	+str(z)+','	+str(w)+' z'}	
		inkex.etree.SubElement(parent, inkex.addNS('path','svg'), attribs)
	
	def calculatePoint(self,x1,y1,x2,y2,distance,angle, layer):
		'''
		Calculate the end points of the 'cut out' for the sensors.
		An arrow shape is created with the top on the bezier curve.
		'''
		if ((x2-x1)==0 and (y2>y1)):
			baseAngle=90
		elif((x2-x1)==0 and (y2<y1)):
			baseAngle=270
		elif((y2-y1)==0 and (x2<x1)):
			baseAngle=180
		else:
			baseAngle=math.degrees(math.atan((y2-y1)/float(x2-x1)))
			baseAngle = abs(baseAngle)
		
		if((y1<y2)and(x1<x2)):
			baseAngle=baseAngle+0
		elif((y1<y2)and(x1>x2)):
			baseAngle=180-baseAngle
		elif((y1>y2)and(x1>x2)):
			baseAngle=baseAngle+180
		elif((y1>y2)and(x1<x2)):
			baseAngle=360-baseAngle
		    
		x3=x2-(distance*math.cos(math.radians(float(baseAngle)+angle)))
		y3=y2-(distance*math.sin(math.radians(float(baseAngle)+angle)))
		    
		x4=x2-(distance*math.cos(math.radians(angle-float(baseAngle))))
		y4=y2+(distance*math.sin(math.radians(angle-float(baseAngle))))

		p=[int(x4),int(y4)]
		
		self.drawLine(x2,y2,x3,y3, layer,'#FFFFFF')
		self.drawLine(x2,y2,x4,y4, layer,'#FFFFFF')
		
        
	def drawLine(self,p1x,p1y,p2x,p2y, layer,color='#000000'):
		'''
		draw the white line in order to separate the sensors
		'''
		path = Path()
		path.addNodeAtEnd(p1x,p1y)
		path.addNodeAtEnd(p2x,p2y)
		
		style = {'stroke' : str(color), 'stroke-width': self.gap, 'fill': 'none'}
		svgPath = inkex.etree.SubElement(layer, inkex.addNS('path','svg'), {'style': simplestyle.formatStyle(style)})
		path.toSVG(svgPath)
			
	def remove(self, element):
		""" Remove the specified element from the document tree """
		parent = self.getParentNode(element)
		if parent is None:
			return
		parent.remove(element)		
			
	def effect(self):
		if not len(self.selected) == 1:
			inkex.errormsg("You must select exactly 1 element.")
			return
		
		# Get script's values
		sensor_distance = self.options.sensor_distance		#defines where the lines are cut out
		arrow_size = self.options.line_thickness + 10
		self.gap = self.options.gap
		line_thickness = self.options.line_thickness
		
		# Get access to main SVG document element and get its dimensions.
		svg = self.document.getroot()
        
        # Create a new layer.
		layer = inkex.etree.SubElement(svg, 'g')
		layer.set(inkex.addNS('label', 'inkscape'), 'CurveLayer')
		layer.set(inkex.addNS('groupmode', 'inkscape'), 'layer')
		
		converter = Converter()
        
		# from Inkscape svg to Path
		userInputObject = self.selected.values()[0]
		pathMask = Path(userInputObject)		
		
		degree = pathMask.getNumNodes()
		nodes = []
		points = []
		
		for i in range(0, degree):
			#Get the nodes from the control graph
			nodes.append(pathMask.getNodeXY(i))
		curve = Bezier(nodes)
		
		i = (100-sensor_distance) /1000.0
		for t in arange(0, 1+i, i):
			point = curve.point(t)
			points.append(point)
		
		#draw the bezier curve
		i = 0.005
		p0 = curve.point(0)
		for t in arange(0, 1, i):
			point = curve.point(t+i)
			self.draw_line(line_thickness, (p0[0], p0[1]), (point[0], point[1]), layer)
			p0 = curve.point(t)
		
		#cut out the sensors
		z = points[0]
		for p in points[1:]:
			self.calculatePoint(z[0],z[1],p[0],p[1],arrow_size,45, layer)
			z = p
			
		""" remove the line which describes the curve """
		for selected_element in self.selected.itervalues():
			self.remove(selected_element)

	

effect = PatternOnCurveEffect()
effect.affect()
