#!/usr/bin/env python

import inkex, simplestyle
from printedelectronics import util
from printedelectronics.pathhelper import Path
from printedelectronics.pathshapelyconverter import Converter
from shapely.geometry.polygon import Polygon

class SmartMaskMultitouchEffect(inkex.Effect):
    """
    Creates a multitouch sensor consisting
    of a grid of sensors. Additionaly
    applies the currently selected object as 
    a mask on the grid (all parts of the
    grid outside the selected object are removed).
    Since in the resulting grid some sensors
    might be very small, it tries to merge
    small sensors into larger sensors. Internally
    it tries to minimize the error function
    
    sum_{sensor in sensors} (|sensor.area - expectedSensorArea|)^e
    
    where e is user specified and expectedSensorArea is the size
    of a sensor if it is not cut by the mask. This effect is slow,
    so be a bit patient.
    
    In contrast to the MaskMultitouchEffect and MulitouchEffect
    this effect does not generate a grid of rects, but the inverse, 
    the paddings between the rects which are then substracted from
    the background. During the optimization process paddings are 
    removed greedily (removing the padding that reduces the error
    most). This is repeated until no more improvement is possible. 
    It might not find the global optimum. The runtime of this approach 
    is aweful. Currently only a simple heuristic is implemented, 
    but improvement might be possible.
    
    @authors: Michael Hedderich & Lena Hegemann
    @date: 2015/12/14
    """
    
    def __init__(self):
        inkex.Effect.__init__(self)
        
        # Options
        self.OptionParser.add_option('-x', '--sensors_x', action = 'store',
          type = 'int', dest = 'sensors_x', default = 50,
          help = 'x dimension')

        self.OptionParser.add_option('-y', '--sensors_y', action = 'store',
          type = 'int', dest = 'sensors_y', default = 50,
          help = 'y dimension')

        self.OptionParser.add_option('-p', '--padding', action = 'store',
          type = 'int', dest = 'padding', default = 20,
          help = 'padding')

        self.OptionParser.add_option('-e', '--errorPower', action = 'store',
          type = 'float', dest = 'errorPower', default = 0.5,
          help = 'sensitivity')
          
        #Tab is only used for design purposes, but Inkscape sends this as another parameter  
        self.OptionParser.add_option('--tab', action = 'store', type = 'string', dest = 'ignore')

    def applyPaddings(self, polyPaddings, background):
        """
        Create a union of the paddings and then substract
        the paddings from the background. Returns
        the resulting Polygon/MultiPolygon
        """
        bigPaddingPolygon = Polygon() 
        for polyPadding in polyPaddings:
            bigPaddingPolygon = bigPaddingPolygon.union(polyPadding)
        
        rects = background.difference(bigPaddingPolygon)
        return rects
        
    def calculateError(self, polygons, expectedArea, power):
        """
        Calculate the error function 
        
        sum_{polygon in polygons} (|polygon.area - expectedSensorArea|)^power
        
        Changing the power might change the result because the difference
        is weighted non-linerarly.
        """
        error = 0
        for polygon in polygons:
            error += abs(polygon.area - expectedArea)**power
        return error

    def findBestMerge(self, polyPaddings, backgroundPolygon, expectedSensorArea, ignoreList, upperError, errorPower):
        """
        Finds the best merge that can be obtained by removing one padding. Paddings in the ignoreList 
        are not tested. If a merge results in an error that is greater than upperError, the padding
        is added to the ignoreList.
        """
        
        bestError = float("inf")
        bestSublist = []
        bestRects = []
        
        #for each possible padding
        for i in range(len(polyPaddings)):
            #check ignore list
            if polyPaddings[i] in ignoreList:
                continue
            
            #create a grid without the current padding
            sublist = polyPaddings[:i]
            sublist.extend(polyPaddings[i+1:])
            rectPolygons = self.applyPaddings(sublist, backgroundPolygon)
            error = self.calculateError(util.cleanUpPolygons([rectPolygons]), expectedSensorArea, errorPower)

            # error is worse than upper bound -> ignore in future steps (simple heuristic)
            if error > upperError:
                ignoreList.append(polyPaddings[i])
            # removing this padding is best solution until now
            elif error < bestError:
                bestError = error
                bestSublist = sublist
                bestRects = rectPolygons
        
        return [bestError, bestSublist, bestRects]

    def effect(self):
        if not len(self.selected) == 1:
            inkex.errormsg("You must select exactly 1 element.")
            return
        
        converter = Converter()
        
        svg = self.document.getroot()

        # Create a new layer.
        layer = inkex.etree.SubElement(svg, 'g')
        layer.set(inkex.addNS('label', 'inkscape'), 'SensorLayer')
        layer.set(inkex.addNS('groupmode', 'inkscape'), 'layer')
        
        # Users might create invalid masks that the Path object can not handle.
        if not Path.isValid(self.selected.values()[0]):
            inkex.errormsg("The selected mask is invalid and can not be handled by the Path object. A reason might be that it contains curved lines. Only straight lines are supported.")
            return
        
        # From Inkscape svg to Path
        pathMask = Path(self.selected.values()[0])
        # From Path to Shapely Polygon
        polyMask = converter.pathToPolygon(pathMask)
        
        # Users might create invalid masks that Shapely can not handle.
        if not polyMask.is_valid:
            inkex.errormsg("The selected mask is invalid and can not be handled by the Shapely library. A reason might be crossing lines.")
            return
        
        # Bounding box of the mask is upper size limit for grid
        minx, miny, maxx, maxy = polyMask.bounds
        maskWidth = maxx - minx
        maskHeight = maxy - miny
        
        sensorWidth = self.options.sensors_x
        sensorHeight = self.options.sensors_y
        padding = self.options.padding
        errorPower = self.options.errorPower
        convergenceThreshold = 0 #improvement in each round must be > this threshold. A value > 0 could be chosen to stop earlier, but usually convergence is reached within a small number of steps (although each step takes a lot of time) 
        
        sensorNumX = int((maskWidth + padding)/(sensorWidth + padding)) + 1
        sensorNumY = int((maskHeight + padding)/(sensorHeight + padding)) + 1
        
        expectedSensorArea = sensorHeight * sensorWidth #area of a sensor if it is not cut by the mask
        
        paddings = util.createPaddingGrid(sensorNumX, sensorNumY, sensorWidth, sensorHeight, padding, minx, miny)
        polyPaddings = [converter.pathToPolygon(path) for path in paddings]

        bigBackgroundPolygon = Polygon([(minx, miny), (maxx, miny), (maxx, maxy), (minx, maxy)]) #big rect the size of the bounding box of the mask
        bigMaskedBackgroundPolygon = bigBackgroundPolygon.intersection(polyMask) #apply mask on background rect
                  
        lastBestError = float("inf")
        improvement = float("inf")
        ignoreList = []
        
        #repeat until no more improvement is found
        #in each round one padding is removed, i.e. a merge is done
        while improvement > convergenceThreshold:
            bestError, bestSublist, bestRects = self.findBestMerge(polyPaddings, bigMaskedBackgroundPolygon, expectedSensorArea, ignoreList, lastBestError, errorPower)
            polyPaddings = bestSublist
            improvement = lastBestError - bestError
            lastBestError = bestError
        
        #convert the resulting rects from Shapely Polygons to Path objects
        rectPaths = converter.polygonsToPath([bestRects])
        
        #Path objects to SVG path elements                
        for rectPath in rectPaths:
            style = {'stroke' : 'none', 'stroke-width': '1', 'fill': '#000000'}
            attribs = {'style': simplestyle.formatStyle(style)}
            svgRectPath = inkex.etree.Element(inkex.addNS('path','svg'),attribs)
            rectPath.toSVG(svgRectPath)
            layer.append(svgRectPath)  

effect = SmartMaskMultitouchEffect()
effect.affect()

