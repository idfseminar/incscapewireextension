#!/usr/bin/env python

import inkex
from printedelectronics.pathhelper import Path

class PathExampleEffect(inkex.Effect):
    """
    Expample for the usage of pathhelper.Path. Creates a fixed path for every selected 
    object.

    @authors: Michael Hedderich & Lena Hegemann
    @date: 2015/12/14
    """

    def __init__(self):
        inkex.Effect.__init__(self)


    def effect(self):
        if len(self.selected) == 0:
            inkex.errormsg("you must select at least one element")
            return
            
        for id, svgPath in self.selected.iteritems():
            path = Path()
            
            path.addNodeAtEnd(100,0)
            path.addNode(0, 200, 600)
            
            path.setNodeXY(1, 300, 300)
            
            path.addNodeAtEnd(350, 1000)
            
            path.setIsClosed(True)
            
            inkex.errormsg(str(path.getNodeXY(1)))

            path.toSVG(svgPath)

effect = PathExampleEffect()
effect.affect()

