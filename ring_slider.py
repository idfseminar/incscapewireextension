#!/usr/bin/env python

# These two lines are only needed if you don't put the script directly into
# the installation directory
import sys
sys.path.append('/usr/share/inkscape/extensions')

# We will use the inkex module with the predefined Effect base class.
import inkex
# The simplestyle module provides functions for style parsing.
from simplestyle import *

# currently for getting the value of pi
from math import *

# for all the following math stuff (maybe you need to download it from scipy.org)
import scipy

#just for fun making further development easier
pi     = scipy.pi
dot    = scipy.dot
sin    = scipy.sin
cos    = scipy.cos
ar     = scipy.array
rand   = scipy.rand
arange = scipy.arange
rad    = lambda ang: ang*pi/180                 #lovely lambda: degree to radian


def Rotate(pts,cnt,ang=pi/4):
    '''pts = {} Rotates points(nx2) about center cnt(2) by angle ang(1) in radian'''
    return dot(pts-cnt,ar([[cos(ang),sin(ang)],[-sin(ang),cos(ang)]]))+cnt

class RingSliderEffect(inkex.Effect):

	def __init__(self):
		
		# Call the base class constructor.
		inkex.Effect.__init__(self)

		# Define string option "--what" with "-w" shortcut and default value "World".
		self.OptionParser.add_option('-r', '--radius', action = 'store', type = 'int', 
			dest = 'radius', default = 15, help = 'radius of ring slider')

		self.OptionParser.add_option('-p', '--padding', action = 'store', type = 'int', 
			dest = 'padding', default = 100, help = 'now more like dont know')

		self.OptionParser.add_option('-m', '--method', action = 'store', type = 'string', 
			dest = 'method', default = 'auto', help = 'method of generation')

       	 	self.OptionParser.add_option('-n', '--number_sensors', action = 'store', type = 'int', 
			dest = 'number_sensors', default = 15, help = 'number of sensors')

        	self.OptionParser.add_option('-s', '--sensor_size', action = 'store', type = 'int', 
			dest = 'sensor_size', default = 20, help = 'sensor size')

		self.OptionParser.add_option('-t', '--object_type', action = 'store', type = 'string', 
			dest = 'object_type', default = 'triangle', help = 'selection of sensor type')

		self.OptionParser.add_option('-z', '--arrow_type', action = 'store', type = 'string', 
			dest = 'arrow_type', default = 'normal', help = 'selection of sensor type')

		self.OptionParser.add_option('--p1_x', action = 'store', type = 'int', 
			dest = 'p1_x', default = '0', help = 'x-coord of point 1 of 6 sided arrow')
		self.OptionParser.add_option('--p1_y', action = 'store', type = 'int', 
			dest = 'p1_y', default = '0', help = 'y-coord of point 1 of 6 sided arrow')
		self.OptionParser.add_option('--p2_x', action = 'store', type = 'int', 
			dest = 'p2_x', default = '66', help = 'x-coord of point 2 of 6 sided arrow')
		self.OptionParser.add_option('--p2_y', action = 'store', type = 'int',  
			dest = 'p2_y', default = '0', help = 'y-coord of point 2 of 6 sided arrow')
		self.OptionParser.add_option('--p3_x', action = 'store', type = 'int',  
			dest = 'p3_x', default = '100', help = 'x-coord of point 3 of 6 sided arrow')
		self.OptionParser.add_option('--p3_y', action = 'store', type = 'int',  
			dest = 'p3_y', default = '50', help = 'y-coord of point 3 of 6 sided arrow')
		self.OptionParser.add_option('--p4_x', action = 'store', type = 'int',  
			dest = 'p4_x', default = '66', help = 'x-coord of point 4 of 6 sided arrow')
		self.OptionParser.add_option('--p4_y', action = 'store', type = 'int', 
			dest = 'p4_y', default = '100', help = 'y-coord of point 4 of 6 sided arrow')
		self.OptionParser.add_option('--p5_x', action = 'store', type = 'int', 
			dest = 'p5_x', default = '0', help = 'x-coord of point 5 of 6 sided arrow')
		self.OptionParser.add_option('--p5_y', action = 'store', type = 'int', 
			dest = 'p5_y', default = '100', help = 'y-coord of point 5 of 6 sided arrow')
		self.OptionParser.add_option('--p6_x', action = 'store', type = 'int', 
			dest = 'p6_x', default = '33', help = 'x-coord of point 6 of 6 sided arrow')
		self.OptionParser.add_option('--p6_y', action = 'store', type = 'int', 
			dest = 'p6_y', default = '50', help = 'y-coord of point 6 of 6 sided arrow')
	
		# Tab is only used for design purposes, but Inkscape sends this as another parameter  
        	self.OptionParser.add_option('--tab', action = 'store', type = 'string', dest = 'ignore')


	def effect(self):

		# Get script's values
		radius = self.options.radius
		number_sensors = self.options.number_sensors
		method = self.options.method
		sensor_size = self.options.sensor_size
		padding = self.options.padding
		object_type = self.options.object_type

		# for arrow 6
		arrow_type = self.options.arrow_type
		p1_x = self.options.p1_x / 100.0
		p1_y = self.options.p1_y / 100.0
		p2_x = self.options.p2_x / 100.0
		p2_y = self.options.p2_y / 100.0
		p3_x = self.options.p3_x / 100.0
		p3_y = self.options.p3_y / 100.0
		p4_x = self.options.p4_x / 100.0
		p4_y = self.options.p4_y / 100.0
		p5_x = self.options.p5_x / 100.0
		p5_y = self.options.p5_y / 100.0
		p6_x = self.options.p6_x / 100.0
		p6_y = self.options.p6_y / 100.0

		multiplikator = padding / 10.0
		offset = radius * 2
		

		padding = number_sensors * (padding / 100.0)
		rotation = rotation = number_sensors* 0.5

		if(method == 'auto'):
			offset = sensor_size * 2;
			radius = (number_sensors*sensor_size) / pi
			multiplikator = 0.7
			

		else:
			radius = (radius*radius) / pi
			multiplikator = 1.0

		if(arrow_type == "normal"):
			p1_x = 0
			p1_y = 0
			p2_x = 0.66
			p2_y = 0
			p3_x = 1.0
			p3_y = 0.5
			p4_x = 0.66
			p4_y = 1.0
			p5_x = 0.0
			p5_y = 1.0
			p6_x = 0.3
			p6_y = 0.5


		# Get access to main SVG document element and get its dimensions.
		svg = self.document.getroot()
		# or alternatively
		# svg = self.document.xpath('//svg:svg',namespaces=inkex.NSS)[0]

		# Again, there are two ways to get the attibutes:
		#width  = self.unittouu(svg.get('width'))
		#height = self.unittouu(svg.attrib['height'])

		# Create a new layer.
		layer = inkex.etree.SubElement(svg, 'g')
		layer.set(inkex.addNS('label', 'inkscape'), 'SensorLayer')
		layer.set(inkex.addNS('groupmode', 'inkscape'), 'layer')
		
		# Create rect element
		style = {'stroke' : 'none', 'stroke-width': '1', 'fill': '#000000'}

		

		if(object_type == 'triangle'):
			# The standard form (right now a normal triangle)
			p1 = [3*offset,offset]
			p2 = [3*offset+sensor_size, offset+sensor_size/2]
			p3 = [3*offset,offset+sensor_size]
			pts = ar([p1, p2, p3])

			for i in arange(0, number_sensors, 1):

				if(i > padding):
					break;

				ang = i*(pi/rotation);

				# the rotation point
				pr = [3*offset, offset+(multiplikator*radius)]

				# python
    				ots = Rotate(pts,ar(pr),ang) 

         			tri_attribs = {'style':formatStyle(style),
					'd':'M '+str(ots[0][0])+','	+str(ots[0][1])+	# p1
					' L '	+str(ots[1][0])+','	+str(ots[1][1])+	# p2
					' L '	+str(ots[2][0])+','	+str(ots[2][1])+	# p3
					' L '	+str(ots[0][0])+','	+str(ots[0][1])+' z'}   # to p1
				inkex.etree.SubElement(layer, inkex.addNS('path','svg'), tri_attribs)

		elif(object_type == 'arrow_4'):

			# normal arrow 4
			#p1 = [3*offset,offset]
			#p2 = [3*offset+sensor_size, offset+sensor_size/2]
			#p3 = [3*offset,offset+sensor_size]
			#p4 = [3*offset+0.33*sensor_size, offset+sensor_size/2]
	
			# curved arrow 4
			p1 = [offset+-0.33*sensor_size, offset+sensor_size]
			p2 = [offset+sensor_size, offset+sensor_size/6]
			p3 = [offset+0.5*sensor_size, offset+1.5*sensor_size]
			p4 = [offset+0.5*sensor_size, offset+sensor_size/1.5]
			pts = ar([p1, p2, p3, p4])

			for i in arange(0, number_sensors, 1):
			
				if(i > padding):
					break;

				ang = i*(pi/rotation);

				# the rotation point
				pr = [offset+(multiplikator*radius), offset+(multiplikator*radius)]

				# python
    				ots = Rotate(pts,ar(pr),ang) 

         			tri_attribs = {'style':formatStyle(style),
					'd':'M '+str(ots[0][0])+','	+str(ots[0][1])+	# p1
					' L '	+str(ots[1][0])+','	+str(ots[1][1])+	# p2
					' L '	+str(ots[2][0])+','	+str(ots[2][1])+	# p3
					' L '	+str(ots[3][0])+','	+str(ots[3][1])+	# p4
					' L '	+str(ots[0][0])+','	+str(ots[0][1])+' z'}	# to p1
				inkex.etree.SubElement(layer, inkex.addNS('path','svg'), tri_attribs)
		
		elif(object_type == 'arrow_6'):

			# normal arrow 6
			p1 = [3*offset + p1_x*sensor_size, offset + p1_y*sensor_size]
			p2 = [3*offset + p2_x*sensor_size, offset + p2_y*sensor_size]
			p3 = [3*offset + p3_x*sensor_size, offset + p3_y*sensor_size]
			p4 = [3*offset + p4_x*sensor_size, offset + p4_y*sensor_size]
			p5 = [3*offset + p5_x*sensor_size, offset + p5_y*sensor_size]
			p6 = [3*offset + p6_x*sensor_size, offset + p6_y*sensor_size]

			pts = ar([p1, p2, p3, p4, p5, p6])

			for i in arange(0, number_sensors, 1):

				if(i > padding):
					break;

				ang = i*(pi/rotation);

				# the rotation point
				pr = [3*offset, offset+(multiplikator*radius)]

				# python
    				ots = Rotate(pts,ar(pr),ang) 

         			tri_attribs = {'style':formatStyle(style),
					'd':'M '+str(ots[0][0])+','	+str(ots[0][1])+	# p1
					' L '	+str(ots[1][0])+','	+str(ots[1][1])+	# p2
					' L '	+str(ots[2][0])+','	+str(ots[2][1])+	# p3
					' L '	+str(ots[3][0])+','	+str(ots[3][1])+	# p4
					' L '	+str(ots[4][0])+','	+str(ots[4][1])+	# p5
					' L '	+str(ots[5][0])+','	+str(ots[5][1])+	# p6
					' L '	+str(ots[0][0])+','	+str(ots[0][1])+' z'}	# to p1
				inkex.etree.SubElement(layer, inkex.addNS('path','svg'), tri_attribs)
		

		# Set text position to center of document.
		# text.set('x', str(width / 2))
		# text.set('y', str(height / 2))

# Create effect instance and apply it.
effect = RingSliderEffect()
effect.affect()
