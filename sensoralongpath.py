#!/usr/bin/env python
'''
Copyright (C) 2006 Jean-Francois Barraud, barraud@math.univ-lille1.fr
Extended by: 2016 Andreas Thieser, s9asthie@stud.uni-saarland.de

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
barraud@math.univ-lille1.fr
'''
# standard library
import copy
import math
import re
import random
# local library
import inkex
import cubicsuperpath
import bezmisc
import pathmodifier
import simpletransform

from simplestyle import *

inkex.localize()

def flipxy(path):
    for pathcomp in path:
        for ctl in pathcomp:
            for pt in ctl:
                tmp=pt[0]
                pt[0]=-pt[1]
                pt[1]=-tmp

def offset(pathcomp,dx,dy):
    for ctl in pathcomp:
        for pt in ctl:
            pt[0]+=dx
            pt[1]+=dy

def stretch(pathcomp,xscale,yscale,org):
    for ctl in pathcomp:
        for pt in ctl:
            pt[0]=org[0]+(pt[0]-org[0])*xscale
            pt[1]=org[1]+(pt[1]-org[1])*yscale

def linearize(p,tolerance=0.001):
    '''
    This function recieves a component of a 'cubicsuperpath' and returns two things:
    The path subdivided in many straight segments, and an array containing the length of each segment.
    
    We could work with bezier path as well, but bezier arc lengths are (re)computed for each point 
    in the deformed object. For complex paths, this might take a while.
    '''
    zero=0.000001
    i=0
    d=0
    lengths=[]
    while i<len(p)-1:
        box  = bezmisc.pointdistance(p[i  ][1],p[i  ][2])
        box += bezmisc.pointdistance(p[i  ][2],p[i+1][0])
        box += bezmisc.pointdistance(p[i+1][0],p[i+1][1])
        chord = bezmisc.pointdistance(p[i][1], p[i+1][1])
        if (box - chord) > tolerance:
            b1, b2 = bezmisc.beziersplitatt([p[i][1],p[i][2],p[i+1][0],p[i+1][1]], 0.5)
            p[i  ][2][0],p[i  ][2][1]=b1[1]
            p[i+1][0][0],p[i+1][0][1]=b2[2]
            p.insert(i+1,[[b1[2][0],b1[2][1]],[b1[3][0],b1[3][1]],[b2[1][0],b2[1][1]]])
        else:
            d=(box+chord)/2
            lengths.append(d)
            i+=1
    new=[p[i][1] for i in range(0,len(p)-1) if lengths[i]>zero]
    new.append(p[-1][1])
    lengths=[l for l in lengths if l>zero]
    return(new,lengths)

class PathAlongPath(pathmodifier.Diffeo):
    def __init__(self):
        pathmodifier.Diffeo.__init__(self)

        self.OptionParser.add_option("--tab",
                        action="store", type="string",
                        dest="tab",
                        help="The selected UI-tab when OK was pressed")

        self.OptionParser.add_option("--sensor_type",
                        action = "store", type = "string",
                        dest = "sensor_type", default = "sensor_arrow",
                        help = "sensor type")

        self.OptionParser.add_option("--sensor_size",
                        action = "store", type = "int",
                        dest = "sensor_size", default = 50,
                        help = "sensor size")

        self.OptionParser.add_option("--padding",
                        action="store", type="float", 
                        dest="padding", default = 0.0,
                        help="sensor padding")

        self.OptionParser.add_option("--overlap",
                        action="store", type="int", 
                        dest="overlap", default = 1,
                        help="anti overlap")

        self.OptionParser.add_option("--flip",
                        action="store", type="inkbool", 
                        dest="flip", default = False,
                        help="flip sensor")

    '''
        Override parents objectToPath method to support conversion from circle/ellipse to a path object
    '''
    def objectToPath(self, node, doReplace=True):
        if node.tag == inkex.addNS('circle', 'svg') or node.tag == inkex.addNS('ellipse', 'svg'):
            return self.ellipseToPath(node, doReplace)
        else:
            return pathmodifier.Diffeo.objectToPath(self, node, doReplace)

    '''
        Converts a circle/ellipse to a path object
    '''
    def ellipseToPath(self,node,doReplace=True):
        if node.tag == inkex.addNS('circle','svg') or node.tag == inkex.addNS('ellipse','svg'):
            flt_cx = float(node.get("cx"))
            flt_cy = float(node.get("cy"))
            if node.tag == inkex.addNS('circle','svg'):
                flt_rx = flt_ry = float(node.get("r"))
            else:
                flt_rx = float(node.get("rx"))
                flt_ry = float(node.get("ry"))

            d ='M %f,%f '%(flt_cx + flt_rx, flt_cy)
            for int_angle in range(1, 360):
                flt_radiant = math.radians(int_angle)
                d += 'L %f,%f ' % (flt_cx + math.cos(flt_radiant) * flt_rx, flt_cy + math.sin(flt_radiant) * flt_ry)
            d += 'Z'

            newnode=inkex.etree.Element('path')
            newnode.set('d',d)
            newnode.set('id', self.uniqueId('path'))
            newnode.set('style',node.get('style'))
            nnt = node.get('transform')
            if nnt:
                newnode.set('transform',nnt)
                fuseTransform(newnode)
            if doReplace:
                parent=node.getparent()
                parent.insert(parent.index(node),newnode)
                parent.remove(node)
            return newnode

    def prepareSelectionList(self):
        sensor_size = self.options.sensor_size
        sensor_type = self.options.sensor_type

        svg = self.document.getroot()

        layer = inkex.etree.SubElement(svg, 'g')
        layer.set(inkex.addNS('label', 'inkscape'), 'SensorLayer')
        layer.set(inkex.addNS('groupmode', 'inkscape'), 'layer')

        style = {'stroke': '#FFFFFF', 'stroke-width': str(self.options.overlap), 'fill': '#000000'}

        if sensor_type == 'sensor_arrow':
            if self.options.flip:
                attribs = {'style': formatStyle(style),
                    'd': 'M ' + str(0.33 * sensor_size) + ', 0' +
                    ' L ' + str(sensor_size) + ', 0' +
                    ' L ' + str(0.66 * sensor_size) + ', ' + str(0.5 * sensor_size) +
                    ' L ' + str(sensor_size) + ', ' + str(sensor_size) +
                    ' L ' + str(0.33 * sensor_size) + ', ' + str(sensor_size) +
                    ' L 0, ' + str(0.5 * sensor_size) +
                    ' L ' + str(0.33 * sensor_size) + ', 0' + ' z'}
            else:
                attribs = {'style': formatStyle(style),
                    'd': 'M 0, 0' +
                    ' L ' + str(0.66 * sensor_size) + ', 0' +
                    ' L ' + str(sensor_size) + ', ' + str(0.5 * sensor_size) +
                    ' L ' + str(0.66 * sensor_size) + ', ' + str(sensor_size) +
                    ' L 0, ' + str(sensor_size) +
                    ' L ' + str(0.33 * sensor_size) + ', ' + str(0.5 * sensor_size) +
                    ' L 0, 0 z'}
            sensor = inkex.etree.SubElement(layer, inkex.addNS('path','svg'), attribs)

        elif sensor_type == 'sensor_square':
            attribs = {'style': formatStyle(style),
                'height': str(sensor_size),
                'width': str(sensor_size),
                'x': '0',
                'y': '0'}
            sensor = inkex.etree.SubElement(layer, inkex.addNS('rect','svg'), attribs)

        elif sensor_type == 'sensor_triangle':
            if self.options.flip:
                attribs = {'style': formatStyle(style),
                    'd': 'M 0, ' + str(sensor_size / 2) +
                    ' L ' + str(sensor_size) + ', ' + str(sensor_size) +
                    ' L ' + str(sensor_size) + ', 0' +
                    ' L 0, ' + str(sensor_size / 2) + ' z'}
            else:
                attribs = {'style': formatStyle(style),
                    'd': 'M 0, 0' +
                    ' L 0, ' + str(sensor_size) +
                    ' L ' + str(sensor_size) + ', ' + str(sensor_size / 2) +
                    ' L 0, 0 z'}
            sensor = inkex.etree.SubElement(layer, inkex.addNS('path','svg'), attribs)

        self.patterns={"s":sensor}
        self.expandGroupsUnlinkClones(self.patterns, True, True)
        self.objectsToPaths(self.patterns)

        self.skeletons=self.selected
        self.expandGroupsUnlinkClones(self.skeletons, True, False)
        self.objectsToPaths(self.skeletons)

    def lengthtotime(self,l):
        '''
        Recieves an arc length l, and returns the index of the segment in self.skelcomp 
        containing the coresponding point, to gether with the position of the point on this segment.

        If the deformer is closed, do computations modulo the toal length.
        '''
        if self.skelcompIsClosed:
            l=l % sum(self.lengths)
        if l<=0:
            return 0,l/self.lengths[0]
        i=0
        while (i<len(self.lengths)) and (self.lengths[i]<=l):
            l-=self.lengths[i]
            i+=1
        t=l/self.lengths[min(i,len(self.lengths)-1)]
        return i, t

    def applyDiffeo(self,bpt,vects=()):
        '''
        The kernel of this stuff:
        bpt is a base point and for v in vectors, v'=v-p is a tangent vector at bpt.
        '''
        s=bpt[0]-self.skelcomp[0][0]
        i,t=self.lengthtotime(s)
        if i==len(self.skelcomp)-1:
            x,y=bezmisc.tpoint(self.skelcomp[i-1],self.skelcomp[i],1+t)
            dx=(self.skelcomp[i][0]-self.skelcomp[i-1][0])/self.lengths[-1]
            dy=(self.skelcomp[i][1]-self.skelcomp[i-1][1])/self.lengths[-1]
        else:
            x,y=bezmisc.tpoint(self.skelcomp[i],self.skelcomp[i+1],t)
            dx=(self.skelcomp[i+1][0]-self.skelcomp[i][0])/self.lengths[i]
            dy=(self.skelcomp[i+1][1]-self.skelcomp[i][1])/self.lengths[i]

        vx=0
        vy=bpt[1]-self.skelcomp[0][1]
        bpt[0]=x+vx*dx-vy*dy
        bpt[1]=y+vx*dy+vy*dx

        for v in vects:
            vx=v[0]-self.skelcomp[0][0]-s
            vy=v[1]-self.skelcomp[0][1]
            v[0]=x+vx*dx-vy*dy
            v[1]=y+vx*dy+vy*dx

    def effect(self):
        if len(self.options.ids)!=1:
            inkex.errormsg(_("This extension requires one selected path."))
            return
        self.prepareSelectionList()

        bbox=simpletransform.computeBBox(self.patterns.values())

        width=bbox[1]-bbox[0]
        dx=width+self.options.padding
        if dx < 0.01:
            exit(_("The total length of the pattern is too small :\nPlease choose a larger object or set 'Space between copies' > 0"))

        for id, node in self.patterns.iteritems():
            if node.tag == inkex.addNS('path','svg') or node.tag=='path':
                d = node.get('d')
                p0 = cubicsuperpath.parsePath(d)

                newp=[]
                for skelnode in self.skeletons.itervalues(): 
                    self.curSekeleton=cubicsuperpath.parsePath(skelnode.get('d'))
                    for comp in self.curSekeleton:
                        p=copy.deepcopy(p0)
                        self.skelcomp,self.lengths=linearize(comp)
                        self.skelcompIsClosed = (self.skelcomp[0]==self.skelcomp[-1])

                        length=sum(self.lengths)
                        xoffset=self.skelcomp[0][0]-bbox[0]
                        yoffset=self.skelcomp[0][1]-(bbox[2]+bbox[3])/2

                        NbCopies=max(1,int(round((length+self.options.padding)/dx)))
                        width=dx*NbCopies
                        if not self.skelcompIsClosed:
                            width-=self.options.padding
                        bbox=bbox[0],bbox[0]+width, bbox[2],bbox[3]
                        new=[]
                        for sub in p:
                            for i in range(0,NbCopies,1):
                                new.append(copy.deepcopy(sub))
                                offset(sub,dx,0)
                        p=new

                        for sub in p:
                            offset(sub,xoffset,yoffset)

                        for sub in p:
                            for ctlpt in sub:
                                self.applyDiffeo(ctlpt[1],(ctlpt[0],ctlpt[2]))

                        newp+=p

                node.set('d', cubicsuperpath.formatPath(newp))

        for selected_element in self.selected.itervalues():
            self.remove(selected_element)

    def remove(self, element):
        """ Remove the specified element from the document tree """
        parent = self.getParentNode(element)
        if parent is None:
            return
        parent.remove(element)

if __name__ == '__main__':
    e = PathAlongPath()
    e.affect()
