#!/usr/bin/env python

import copy
import inkex
from simplestyle import *
from printedelectronics import util

class MultitouchSensorEffect(inkex.Effect):
    """
    Creates a multitouch sensor consisting
    of a grid of sensors.
    
    @authors: Michael Hedderich & Lena Hegemann
    @date: 2015/12/14
    """
    def __init__(self):
        inkex.Effect.__init__(self)

        # Options
        self.OptionParser.add_option('-w', '--object_width', action = 'store',
          type = 'int', dest = 'object_width', default = 100,
          help = 'object width')

        self.OptionParser.add_option('-e', '--object_height', action = 'store',
          type = 'int', dest = 'object_height', default = 100,
          help = 'object height')

        self.OptionParser.add_option('-m', '--mode', action = 'store',
          type = 'string', dest = 'mode', default = 'sensor_size',
          help = 'election mode')

        self.OptionParser.add_option('-x', '--sensors_x', action = 'store',
          type = 'int', dest = 'sensors_x', default = 20,
          help = 'x dimension')

        self.OptionParser.add_option('-y', '--sensors_y', action = 'store',
          type = 'int', dest = 'sensors_y', default = 20,
          help = 'y dimension')

        self.OptionParser.add_option('-p', '--padding', action = 'store',
          type = 'int', dest = 'padding', default = 10,
          help = 'padding')
        
        #Tab is only used for design purposes, but Inkscape sends this as another parameter  
        self.OptionParser.add_option('--tab', action = 'store', type = 'string', dest = 'ignore')

    def effect(self):
        # Get user input
        object_width = self.options.object_width
        object_height = self.options.object_height
        mode = self.options.mode #two modes: sensor calculations based on sensor_size or sensor_number
        sensors_x = self.options.sensors_x
        sensors_y = self.options.sensors_y
        padding = self.options.padding

        svg = self.document.getroot()

        # Create a new layer.
        layer = inkex.etree.SubElement(svg, 'g')
        layer.set(inkex.addNS('label', 'inkscape'), 'SensorLayer')
        layer.set(inkex.addNS('groupmode', 'inkscape'), 'layer')

        # Compute sensor dimensions and number based on user input
        sensor_width = 0
        sensor_height = 0
        sensor_num_x = 0
        sensor_num_y = 0

        if mode == 'sensor_size':
            sensor_width = sensors_x
            sensor_height = sensors_y
            sensor_num_x = (object_width + padding)/(sensor_width + padding)
            sensor_num_y = (object_height + padding)/(sensor_height + padding)
        elif mode == 'sensor_number':
            sensor_num_x = sensors_x
            sensor_num_y = sensors_y
            sensor_width = (object_width - (sensor_num_x-1)*padding)/(sensor_num_x)
            sensor_height = (object_height - (sensor_num_y-1)*padding)/sensor_num_y

        if sensor_num_x < 1 or sensor_num_y < 1 or sensor_width < 1 or sensor_height < 1:
            inkex.errormsg("Invalid parameters")
        
        #create the grid        
        grid = util.createSensorGrid(sensor_num_x, sensor_num_y, sensor_width, sensor_height, padding, 0, 0)
        
        #convert the paths in the grid into svg elements and add them to the layer        
        for rect in grid:
          style = {'stroke' : 'none', 'stroke-width': '1', 'fill': '#000000'}
          attribs = {'style': formatStyle(style)}
          svgRectPath = inkex.etree.Element(inkex.addNS('path','svg'),attribs)
          rect.toSVG(svgRectPath)
          layer.append(svgRectPath)       

effect = MultitouchSensorEffect()
effect.affect()
