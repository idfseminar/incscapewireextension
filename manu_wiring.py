#!/usr/bin/env python
# Vikram Narayanan
# Magnus Halbe (use of shapely to generate obstacle arrays)

import sys
sys.path.append('/usr/share/inkscape/extensions')
import inkex
import simplestyle
from inkex import debug
import shapely
from shapely.geometry.linestring import LineString
import iterativeMultipleMazeV2
import a_star_maze
from printedelectronics.pathhelper import *
from printedelectronics.pathshapelyconverter import Converter
import math
import copy
from overlay_grid import *
from connectors import *
import operator


class ManuellWirePlacement(inkex.Effect):
    """
    Creates one wire connection between the two selected objects
    """
    def __init__(self):
        # Call the base class constructor.
        inkex.Effect.__init__(self)
        self.OptionParser.add_option('--wire_width', action = 'store',
                                     type = 'float', dest = 'wire_width', default = '2.5',
                                     help = 'Width of the drwan wires')

        self.OptionParser.add_option('--spacing', action = 'store',
                                     type = 'float', dest = 'spacing', default = '2.5',
                                     help = 'Spacing between objects')

        self.OptionParser.add_option('--path_finding_detail', action = 'store',
                                     type = 'string', dest = 'path_finding_detail', default = 'medium',
                                     help = 'Path finding detail')

        self.OptionParser.add_option('--algorithm', action = 'store',
                                     type = 'int', dest = 'used_algo', default = '1',
                                     help = 'Used algorithm for connecting')


        # Tab is only used for design purposes, but Inkscape sends this as another parameter
        self.OptionParser.add_option('--tab', action = 'store', type = 'string', dest = 'ignore')

    @staticmethod
    def draw_path(path_array, width, color, layer):
        """
        Draws a simple path given by an array with x,y coordinates
        :param path_array: point array which build ups the path
        :param width: line width
        :param color: line color
        :param layer: layer on which the path will be placed
        """
        line = Path()
        line.setNodes(path_array)
        style = {'stroke': color,
                 'stroke-width': width,
                 'fill': 'none'}
        attribs = {'style': simplestyle.formatStyle(style)}
        svg_line_path = inkex.etree.SubElement(layer, inkex.addNS('path', 'svg'), attribs)
        line.toSVG(svg_line_path)
        return line

    @staticmethod
    def sort_coord_list(sort_x, list, increasing):
        """
        Sorts a list filled with coordinates (x,y) by x (when sort_vertical == false) or by y otherwise
        :param sort_x: considering the x values when true. Otherwise the y values will be considered
        :param list: coordinate list that have to be sorted
        :param increasing: sorting from small to large if true
        :return: the sorted coord list
        """
        # result = sorted(list, key=getKey)
        if sort_x is True:
            result = sorted(list, key=operator.itemgetter(0, 1))
        else:
            result = sorted(list, key=operator.itemgetter(1, 0))

        if increasing is True:
            return result
        return ManuellWirePlacement.reverse_list(result)

    @staticmethod
    def split_list(size_1, list):
        list_1 = []

        if size_1 > len(list):
            debug('Splitting not possible')
            return False

        helper_list = copy.deepcopy(list)

        while len(list_1) < size_1:
            element = helper_list.pop(0)
            list_1.append(element)

        list_2 = helper_list
        return list_1, list_2


    @staticmethod
    def reverse_list(list):
        """
        Simple method to revert a list
        :param list:
        :return: reverted list
        """
        rev_list = []
        for i in reversed(list):
            rev_list.append(i)
        return rev_list

    @staticmethod
    def list_divide_comonents(list, divisor):
        result = []
        for elem in list:
            result.append(elem / divisor)

        return result

    def lookup_obstacles(self, svgdoc, obstacle_list):
        for child in svgdoc.findall('{http://www.w3.org/2000/svg}g'):
            # Try to find all possible obstacles, export them as shapely
            for elem in child:
                if Path.isValid(elem) is False:
                    debug("invalid object found")
                    return
                else:
                    pathMask = Path(elem)
                    # From Path to Shapely Polygon
                    if 'WireLayer' in child.attrib.values():
                        poly_mask = Converter.pathToLineString(pathMask)
                    else:
                        poly_mask = Converter.pathToLinearRing(pathMask)

                    # Do not set the selected polygons in the obstacle list
                    if not (poly_mask.equals(self.objectpoly_1) or poly_mask.equals(self.objectpoly_2)):
                        obstacle_list.append(poly_mask)

    def connect_points(self, start_point, end_point, logic_grid):
        """
        Connects two (x, y) points in the given grid and also fills the path route in the grid with zeros.
        The line will be drawn on the wire layer
        :param start_point: start point (connoector)
        :param end_point: end point (sensor)
        :param logic_grid: grid, where sensors, wires and connectors are marked with 1
        :returns line_string: A shapely LineString object that represents the created wire
        """
        logic_grid_maze = copy.deepcopy(logic_grid)

        if self.options.used_algo == 1:
            found_path = iterativeMultipleMazeV2.solveMaze(logic_grid_maze, start_point[0], start_point[1], end_point[0], end_point[1])
        elif self.options.used_algo == 2:
            found_path = a_star_maze.solveMaze(logic_grid_maze, start_point[0], start_point[1], end_point[0], end_point[1])

        if found_path is not False:
            real_coord_path = []

            for point in found_path:
                logic_grid[point[0]][point[1]] = 0  # now occupied by path
                real_coord_path.append(((point[0]*self.cellWidth + self.cellWidth/2), (point[1]*self.cellHeight + self.cellHeight/2)))

            line_path = ManuellWirePlacement.draw_path(real_coord_path, self.options.wire_width, '#000000', self.wire_layer)

            return LineString(real_coord_path)
        return False

    # overridden function
    def effect(self):

        if not len(self.selected) == 2:
            inkex.errormsg("You must select exactly 2 elements.")
            return

        if not Path.isValid(self.selected.values()[0]):
            inkex.errormsg("The selected mask is invalid and can not be handled by the Path object.")
            return

        if not Path.isValid(self.selected.values()[1]):
            inkex.errormsg("The selected mask is invalid and can not be handled by the Path object.")
            return

        # Creating shapely objects out of the two connecting objects
        # object_mask_1 = Path(self.selected.values()[0])
        # self.objectpoly_1 = Converter.pathToPolygon(object_mask_1)

        object_mask_1 = Path(self.selected.values()[0])
        self.objectpoly_1 = Converter.pathToLinearRing(object_mask_1)

        object_mask_2 = Path(self.selected.values()[1])
        self.objectpoly_2 = Converter.pathToLinearRing(object_mask_2)

        svgdoc = self.document.getroot()

        obstacle_list = []
        self.lookup_obstacles(svgdoc, obstacle_list)

        destination_list = []
        start_list = []

        # Connection point is in the representative point
        conn_point_2 = self.objectpoly_2.representative_point()
        sensor_obstcale = (conn_point_2.x, conn_point_2.y, self.objectpoly_2)
        destination_list.append(sensor_obstcale)

        conn_point_1 = self.objectpoly_1.centroid
        connector_obstacle = (conn_point_1.x, conn_point_1.y, self.objectpoly_1)
        start_list.append(connector_obstacle)

        # create a new layer for connectors
        clayer = inkex.etree.SubElement(svgdoc, 'g')
        clayer.set(inkex.addNS('label', 'inkscape'), 'ConnectorLayer')
        clayer.set(inkex.addNS('groupmode', 'inkscape'), 'layer')

        svg_width = self.unittouu(svgdoc.get('width'))
        svg_height = self.unittouu(svgdoc.get('height'))

        if self.options.path_finding_detail == "high":
            cell_detail_factor = 1
        elif self.options.path_finding_detail == "medium":
            cell_detail_factor = 5
        else:
            cell_detail_factor = 10

        grid = OverlayGrid(obstacle_list, start_list, destination_list, svg_width, svg_height, self.options.wire_width, self.options.spacing, cell_detail_factor)

        self.cellWidth = grid.cell_width_minimum
        self.cellHeight = grid.cell_height_minimum

        # create layer for the wires
        self.wire_layer = inkex.etree.SubElement(svgdoc, 'g')
        self.wire_layer.set(inkex.addNS('label', 'inkscape'), 'WireLayer')
        self.wire_layer.set(inkex.addNS('groupmode', 'inkscape'), 'layer')

        # Try to connect all start points to the referring destination points

        start = grid.start_list[0]
        dest = grid.dst_list[0]

        start_point = (start[0], start[1])
        dst_point = (dest[0], dest[1])

        ready_grid = grid.get_grid_manipulate_obstacles_id(0, False)

        wire_line_string = self.connect_points(start_point, dst_point, ready_grid)

        grid.logical_grid = ready_grid
        grid.logical_grid = grid.get_grid_manipulate_obstacles_id(0, True)

        if wire_line_string is not False:
            grid.add_wire(wire_line_string) # pass the wire to the shapely space


wplacement = ManuellWirePlacement()
'''
This calls the affect() function from inkex.py which makes
all the changes to the document
'''
wplacement.affect()
