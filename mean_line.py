#!/usr/bin/env python

"""

"""
import sys
import cmath
import math
import pprint

import inkex, simplestyle
from printedelectronics import util
from printedelectronics.pathhelper import Path
from printedelectronics.pathshapelyconverter import Converter
from shapely.geometry.polygon import Polygon
from shapely.geometry.linestring import LineString
from shapely.geometry import Point
import shapely.geometry.base
from shapely.ops import unary_union
from shapely.ops import polygonize

class MeanLine(inkex.Effect):
    
	def __init__(self):
		inkex.Effect.__init__(self)
		
		self.OptionParser.add_option('-s', '--smoothness', action = 'store',
		  type = 'int', dest = 'smoothness', default = 50,
		  help = 'smoothness')
				  
		# Tab is only used for design purposes, but Inkscape sends this as another parameter  
		self.OptionParser.add_option('--tab', action = 'store', type = 'string', dest = 'ignore')
	
	def drawRect(self, x, y, size, color='#000000'):
		style = {'stroke' : str(color), 'stroke-width': '1', 'fill': 'none'}
		attribs = {'height': str(size), 'width': str(size), 'x': str(x), 'y': str(y), 'style': simplestyle.formatStyle(style)}
		inkex.etree.SubElement(layer, inkex.addNS('rect','svg'), attribs)
		
	def drawPoint(self, x, y, color='#000000'):
		self.drawRect(x-1, y-1, 2, color)
		
	def drawPoint(self, point, color='#000000'):
		self.drawRect(point.x-1, point.y-1, 2, color)
		
	def drawPath(self, path, color='#000000'):
		style = {'stroke' : str(color), 'stroke-width': '1', 'fill': 'none'}
		svgPath = inkex.etree.SubElement(layer, inkex.addNS('path','svg'), {'style': simplestyle.formatStyle(style)})
		path.toSVG(svgPath)
		
	def drawLine(self, line, color='#000000'):
		path = Path()
		for coord in line.coords:
			path.addNodeAtEnd(coord[0], coord[1])
		
		self.drawPath(path, color)
		
	def drawNumber(self, x, y, number, size=10, color='#000000'):
		style = {'stroke' : str(color), 'stroke-width': '1', 'fill': 'none'}
		for i in range(number):
			path = Path()
			path.addNodeAtEnd(x+(2*i), y)
			path.addNodeAtEnd(x+(2*i), y+size)
			
			svgPath = inkex.etree.SubElement(layer, inkex.addNS('path','svg'), {'style': simplestyle.formatStyle(style)})
			path.toSVG(svgPath)
		
	def findMeanPoints(self, poly):
		coords = list(poly.exterior.coords)
		# since the last and the first point are equal, remove the last entry
		coords.pop()
		
		centroids = []
		
		# TODO: per point p use only nearest n points ps to form centroids.
		#		best case: only the points directly on the "other side" of the polygon
		
		l = len(coords)
		for i in range(l):
			for j in range(i+2, l):
				# range(i+1, l) and remove if for centroids between neighbour points
				if i == 0 and j == l-1:
					continue
				line = LineString((coords[i],coords[j]))
				centroid = line.centroid
				self.drawPoint(centroid, '#ff0000')
						
				# DONE: filter centroids outside of the polygon
				if centroid.within(poly):
					centroids.append(centroid)
					
		return centroids
		
	def findMeanPoints2(self, poly):
		coords = list(poly.exterior.coords)
		# since the last and the first point are equal, remove the last entry
		coords.pop()
		
		centroids = []
		
		# boundingBox (minx, miny, maxx, maxy)
		boundingBox = poly.bounds
		boundingBoxMin = Point(boundingBox[0], boundingBox[1])
		boundingBoxMax = Point(boundingBox[2], boundingBox[3])
		# use diagonal of boundingBox for the length of the cuting lines.
		# The diagonal represents the longest possible distance inside the polygon
		cutLength = boundingBoxMin.distance(boundingBoxMax)
		
		l = len(coords)
		for i in range(l):
			a = Point(coords[(i-1)%l])
			b = Point(coords[i])
			c = Point(coords[(i+1)%l])
			
			if debug:
				self.drawNumber(b.x-10, b.y-10, i+1)
			
			# direction veectors of the two lines
			ba = self.normalizeVector(Point(a.x-b.x, a.y-b.y))
			bc = self.normalizeVector(Point(c.x-b.x, c.y-b.y))
			
			cross = self.cross(ba, bc)
			
			if cross < 0:
				innerAngle = True
			else:
				innerAngle = False
			
			if debug:
				inkex.errormsg(str(i+1))
				inkex.errormsg("ba " + str(ba))
				inkex.errormsg("bc " + str(bc))
				#inkex.errormsg("cross " + str(cross))
				babcAngle = self.radiantToDegree(self.calculateAngleRAD(ba, bc))
				inkex.errormsg("deg angle ba-bc " + str(babcAngle))
				if innerAngle:
					inkex.errormsg("deg angle inner " + str(babcAngle))
				else:
					inkex.errormsg("deg angle inner " + str(360-babcAngle))
					
			
			# from http://answers.unity3d.com/answers/437126/view.html
			middle = self.normalizeVector(Point(ba.x + bc.x, ba.y + bc.y))
			# if the cross product is positive, the calculated angle is the outer angle.
			# therefore we have to invert the 
			if not innerAngle:
				middle = Point(-middle.x, -middle.y)
			
			if debug:
				inkex.errormsg("middle " + str(middle))
				inkex.errormsg("deg angle ba-middle " + str(self.radiantToDegree(self.calculateAngleRAD(ba, middle))))
				inkex.errormsg("deg angle bc-middle " + str(self.radiantToDegree(self.calculateAngleRAD(bc, middle))))
				inkex.errormsg("---")
				
				self.drawPoint(Point(b.x+middle.x*10, b.y+middle.y*10), '#ff0000')
			
			cutEndpoint = Point(b.x + middle.x*cutLength, b.y + middle.y*cutLength)
			cuttingLine = LineString((b, cutEndpoint))
			
			intersection = self.calculateNearestIntersection(poly.exterior, cuttingLine)
			
			centroid = LineString((b, intersection)).centroid
			
			if debug:
				self.drawPoint(centroid, '#ffa500')
			
			centroids.append(centroid)
		
		return centroids
			
			
	def cross(self, a, b):
		return a.x*b.y - a.y*b.x
		
	def dot(self, a, b):
		return a.x*b.x + a.y*b.y
		
	def radiantToDegree(self, rad):
		return rad*(180/math.pi)
		
	def normalizeVector(self, v):
		norm = cmath.sqrt(pow(v.x, 2) + pow(v.y, 2))
		# due to the square, there is NO imaginary component
		norm = norm.real
		if norm != 0:
			return Point(v.x/norm , v.y/norm)
		else:
			return v
			
	def calculateAngleRAD(self, vector1, vector2):
		# normalize vectors to ensure value of dot is in [-1, 1] --> acos is defined in that range only
		vector1 = self.normalizeVector(vector1)
		vector2 = self.normalizeVector(vector2)
		dot = self.dot(vector1, vector2)
		angle = math.acos(dot)
		return angle
		
	def calculateNearestIntersection(self, polygonExterior, line):
		source = Point(line.coords[0])
		
		intersections = list(polygonExterior.intersection(line))
		
		nearest = (float("inf"), None)
		for inter in intersections:
			if isinstance(inter, Point):
				distance = source.distance(inter)
				if distance > 0 and distance < nearest[0]:
					nearest = (distance, inter)
			elif isinstance(inter, LineString):
				distance = source.distance(inter)
				if distance > 0 and distance < nearest[0]:
					# inter.coord[0] should be the nearest point on the intersection line
					nearestPointOnLine = inter.coords[0]
					nearest = (distance, nearestPointOnLine)
			else:
				# see http://toblerity.org/shapely/manual.html#collections
				pass
		
		return nearest[1]
			
		
	def __comparePoints(self, a, b):
		# from http://stackoverflow.com/a/6989383
		if (a[0] - center[0] >= 0 and b[0] - center[0] < 0):
			return True;
		if (a[0] - center[0] < 0 and b[0] - center[0] >= 0):
			return False
		if (a[0] - center[0] == 0 and b[0] - center[0] == 0):
			if (a[1] - center[1] >= 0 or b[1] - center[1] >= 0):
				return a[1] > b[1]
			return b[1] > a[1]

		# compute the cross product of vectors (center -> a) x (center -> b)
		det = (a[0] - center[0]) * (b[1] - center[1]) - (b[0] - center[0]) * (a[1] - center[1])
		if (det < 0):
			return True
		if (det > 0):
			return False

		# points a and b are on the same line from the center
		# check which point is closer to the center
		d1 = (a[0] - center[0]) * (a[0] - center[0]) + (a[1] - center[1]) * (a[1] - center[1])
		d2 = (b[0] - center[0]) * (b[0] - center[0]) + (b[1] - center[1]) * (b[1] - center[1])
		return d1 > d2;
				
	def comparePoints(self, a, b):
		if self.__comparePoints(a, b):
			return 1
		else:
			return -1
		
	def calculateStraightMeanLine(self, centroids):
		maxDistance = 0
		
		for c1 in centroids:
			for c2 in centroids:
				if c1 == c2:
					continue
				d = c1.distance(c2)
				if d > maxDistance:
					maxDistance = d
					line = LineString((c1, c2))
					
		return line
		
	def projectPointOnLine(self, line, point):
		# TODO: use shapely methods as described on http://stackoverflow.com/a/24440122
		
		# from http://stackoverflow.com/a/15232899
		P1 = Point(line.coords[0])
		P2 = Point(line.coords[1])
		P3 = point
		
		isValid = False
		
		if P1.y == P2.y and P1.x == P2.x:
			P1.y -= 0.00001;

		U = ((P3.y - P1.y) * (P2.y - P1.y)) + ((P3.x - P1.x) * (P2.x - P1.x))

		Udenom = pow(P2.y - P1.y, 2) + pow(P2.x - P1.x, 2)

		U /= Udenom

		ry = P1.y + (U * (P2.y - P1.y))
		rx = P1.x + (U * (P2.x - P1.x))
		r = Point(rx, ry)

		minx = min(P1.y, P2.y);
		maxx = max(P1.y, P2.y);

		miny = min(P1.x, P2.x);
		maxy = max(P1.x, P2.x);

		isValid = (r.y >= minx and r.y <= maxx) or (r.x >= miny and r.x <= maxy);

		if isValid:
			return r
		else:
			return None
		
	def projectPointsOnLine(self, line, points):
		projections = []
		for point in points:
			projection = self.projectPointOnLine(line, point)
			self.drawPoint(projection, '#00ff00')
			self.drawLine(LineString((point, projection)), '#ffa500')
			projections.append(projection)
		
		return projections
			
	def calculatePointOnLine(self, line, distance):
		a = Point(line.coords[0])
		b = Point(line.coords[1])
		lineDirection = Point(b.x-a.x, b.y-a.y)
		
		lineDirection = self.normalizeVector(lineDirection)
		
		point = Point(a.x + (lineDirection.x * distance), a.y + (lineDirection.y * distance))
		
		return point
		
	def mergeClosePoints(self, list, minDistance):
		mergedPoints = []
		
		while len(list) > 0:
			p1 = list.pop()
			# if this is the last point in the list, there is no close point left
			if len(list) == 0:
				mergedPoints.append(p1)
				continue
			
			i = 0
			while i < len(list):
				p2 = list[i]
				distance = p1.distance(p2)
				if distance <= minDistance:
					# calculate new point and put it in the list
					# TODO: if this new point is in minDistance to another centroid,
					# 		result won't be the correct combination of all three centroids
					newPoint = Point((p1.x + p2.x)/2, (p1.y + p2.y)/2)
					list.append(newPoint)
					
					# now p1 and p2 are represented by the newPoint
					# remove p2 from list, p1 was already removed
					list.pop(i)
					
					# continue with next p1 in list
					# NO need to increment i, point on position i was removed
					break
				else:
					# p2 is not close enough, continue with next point
					i += 1
					if i == len(list):
						# if there is no next point, then there is no close neighbour either
						mergedPoints.append(p1)
			
		return mergedPoints
	
	def effect(self):
		if not len(self.selected) == 1:
			inkex.errormsg("You must select exactly 1 element.")
			return

		converter = Converter()

		svg = self.document.getroot()

		# Create a new layer.
		global layer
		layer = inkex.etree.SubElement(svg, 'g')
		layer.set(inkex.addNS('label', 'inkscape'), 'SensorLayer')
		layer.set(inkex.addNS('groupmode', 'inkscape'), 'layer')
		
		userInputObject = self.selected.values()[0]
		if not Path.isValid(userInputObject):
			inkex.errormsg("The selected object is invalid and can not be handled by the Path object. A reason might be that it contains curved lines. Only straight lines are supported.")
			return
			
		# from Inkscape svg to Path
		pathMask = Path(userInputObject)
		# from Path to Shapely Polygon
		polyMask = converter.pathToPolygon(pathMask)

		if not polyMask.is_valid:
			inkex.errormsg("The selected mask is invalid and can not be handled by the Shapely library. A reason might be crossing lines.")
			return
		
		smoothness = self.options.smoothness
		
		#global pathStyle
		#pathStyle = {'stroke' : '#000000', 'stroke-width': '1', 'fill': 'none'}
		
		# TODO: add flag to extension window
		global debug
		debug = False
		
		""" self.wayOne(polyMask) """
		
		""" wayTwo """
		
		# sort counter-clockwise
		polyCoords = list(polyMask.exterior.coords)
		# since the last and the first point are equal, remove the last entry
		polyCoords.pop()
		
		global center
		center = polyMask.centroid
		center = (center.x, center.y)
		
		# order of the polygon musst be counter-clockwise
		#polyCoordsSorted = sorted(polyCoords, cmp=self.comparePoints)
		#polyMask = Polygon(polyCoordsSorted)
		#self.drawPath(converter.polygonsToPath([polyMask])[0], '#0000ff')
		
		
		centroids = self.findMeanPoints2(polyMask)
		
		# TODO: add slider for smothness (minDistance)
		minDistance = smoothness
		centroids = self.mergeClosePoints(centroids, minDistance)
		
		# draw final points for mean line 
		[self.drawPoint(c, '#00ff00') for c in centroids]
		
		# TODO: depending on type of polygon (convex/non-convex), suitable sorting of the final points is needed
		if len(centroids) > 1:
			meanLine = LineString(centroids)
			self.drawLine(meanLine)
		
		# TODO: check whether wayOne still works
		# TODO: add drop down for choosing algorithm
		
		"""
		meanline = self.calculateStraightMeanLine(centroids)
		self.drawLine(meanline)
		
		projections = self.projectPointsOnLine(meanline, centroids)
		
		# TODO: change weight into function, that punishes higher distance more intense
		
		
		weight = 0.1
		correctedProjections = []
		
		# move projected centroid in direction of actual centroid
		for i in range(len(projections)):
			direction = LineString((projections[i], centroids[i]))
			correctedProjection = self.calculatePointOnLine(direction, weight*direction.length)
			self.drawPoint(correctedProjection, '#0000ff')
			correctedProjections.append(correctedProjection)
		
		
		correctedProjectionsSorted = sorted(correctedProjections, key=lambda point: point.x)
		correctedMeanLine = LineString(correctedProjectionsSorted)
		
		#sorted(student_tuples, key=lambda student: student[2])   # sort by age
		
		self.drawLine(correctedMeanLine, '#a5a5a5')
		
		"""
		
		
		
		#difference = polyConvexHull.difference(polyMask)
		
		#convexHullPathList = converter.polygonsToPath([polyConvexHull, difference])
		
		#pathStyle['stroke'] = '#ff0000'
		#svgRectPath = inkex.etree.SubElement(layer, inkex.addNS('path','svg'), {'style': simplestyle.formatStyle(pathStyle)})
		#convexHullPathList[0].toSVG(svgRectPath)
		
		#pathStyle['stroke'] = '#0000ff'
		#svgRectPath = inkex.etree.SubElement(layer, inkex.addNS('path','svg'), {'style': simplestyle.formatStyle(pathStyle)})
		#convexHullPathList[1].toSVG(svgRectPath)
	
	def wayOne(self, polyMask):
		centroids = self.findMeanPoints(polyMask)
		
		meanline = self.calculateStraightMeanLine(centroids)
		self.drawLine(meanline)
		
		projections = self.projectPointsOnLine(meanline, centroids)
		
		# TODO: change weight into function, that punishes higher distance more intense
		
		# TODO: check whether mergeClosePoints is a good improvement here
		
		weight = 0.1
		correctedProjections = []
		
		# move projected centroid in direction of actual centroid
		for i in range(len(projections)):
			direction = LineString((projections[i], centroids[i]))
			correctedProjection = self.calculatePointOnLine(direction, weight*direction.length)
			self.drawPoint(correctedProjection, '#0000ff')
			correctedProjections.append(correctedProjection)
		
		
		correctedProjectionsSorted = sorted(correctedProjections, key=lambda point: point.x)
		correctedMeanLine = LineString(correctedProjectionsSorted)
		
		#sorted(student_tuples, key=lambda student: student[2])   # sort by age
		
		self.drawLine(correctedMeanLine, '#a5a5a5')
		
		

effect = MeanLine()
effect.affect()

