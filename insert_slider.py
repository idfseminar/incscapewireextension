#!/usr/bin/env python

"""

"""

import inkex, simplestyle
from printedelectronics import util
from printedelectronics.pathhelper import Path
from printedelectronics.pathshapelyconverter import Converter
from shapely.geometry.polygon import Polygon

class InsertSlider(inkex.Effect):
    
    def __init__(self):
		inkex.Effect.__init__(self)

		self.OptionParser.add_option('-w', '--sensorsWidth', action = 'store',
			type = 'int', dest = 'sensorsWidth', default = 20,
			help = 'sensor width')

		self.OptionParser.add_option('-t', '--sensorsAngle', action = 'store',
			type = 'int', dest = 'sensorsAngle', default = 20,
			help = 'sensor angle')

		self.OptionParser.add_option('-p', '--padding', action = 'store',
			type = 'int', dest = 'padding', default = 5,
			help = 'padding')

		self.OptionParser.add_option('-i', '--initialOffset', action = 'store',
			type = 'int', dest = 'initialOffset', default = 0,
			help = 'initial offset')

		self.OptionParser.add_option('-s', '--swapDirection', action = 'store',
			type = 'inkbool', dest = 'swapDirection', default = False,
			help = 'Swap direction')

		# Tab is only used for design purposes, but Inkscape sends this as another parameter  
		self.OptionParser.add_option('--tab', action = 'store', type = 'string', dest = 'ignore')
		
    def effect(self):
		if not len(self.selected) == 1:
			inkex.errormsg("You must select exactly 1 element.")
			return

		converter = Converter()

		svg = self.document.getroot()

		# Create a new layer.
		layer = inkex.etree.SubElement(svg, 'g')
		layer.set(inkex.addNS('label', 'inkscape'), 'SensorLayer')
		layer.set(inkex.addNS('groupmode', 'inkscape'), 'layer')

		userInputObject = self.selected.values()[0]
		if not Path.isValid(userInputObject):
			inkex.errormsg("The selected object is invalid and can not be handled by the Path object. A reason might be that it contains curved lines. Only straight lines are supported.")
			return
			
		# from Inkscape svg to Path
		pathMask = Path(userInputObject)
		# from Path to Shapely Polygon
		polyMask = converter.pathToPolygon(pathMask)

		if not polyMask.is_valid:
			inkex.errormsg("The selected mask is invalid and can not be handled by the Shapely library. A reason might be crossing lines.")
			return
			
		minx, miny, maxx, maxy = polyMask.bounds

		boundsWidth = maxx - minx
		boundsHeight = maxy - miny
		
		# TODO: refactor code, e.g. into separate methods
		
		sensorWidth = self.options.sensorsWidth
		sensorsAngle = self.options.sensorsAngle
		padding = self.options.padding
		initialOffset = self.options.initialOffset
		swapDirection = self.options.swapDirection
		
		# find orientation of object
		if boundsWidth < boundsHeight :
			vertical = True
		else :
			vertical = False
			
		if swapDirection :
			vertical = not vertical
			
		# show orientation with simple path
		#directionPath = Path()
		#if vertical :
		#	directionPath.addNodeAtEnd(minx + (boundsWidth/2), miny)
		#	directionPath.addNodeAtEnd(minx + (boundsWidth/2), maxy)
		#else :
		#	directionPath.addNodeAtEnd(minx, miny + (boundsHeight/2))
		#	directionPath.addNodeAtEnd(maxx, miny + (boundsHeight/2))
		#	
		#pathStyle = {'stroke' : '#000000', 'stroke-width': '1', 'fill': 'none'}
		#pathAttribs = {'style':simplestyle.formatStyle(pathStyle)}
		#directionPath.toSVG(inkex.etree.SubElement(layer, inkex.addNS('path','svg'), pathAttribs ))
		
		# draw sensors
		if vertical:
			distance = sensorWidth + padding
			offset = initialOffset
			
			if padding < sensorWidth/2 :
				offset -= distance
			
			sensorPaths = []
			
			while offset < boundsHeight :
				trianglePath = Path()
				trianglePath.addNodeAtEnd(minx,					offset + miny)
				trianglePath.addNodeAtEnd(minx,					offset + miny + sensorWidth)
				trianglePath.addNodeAtEnd(minx + boundsWidth/2,	offset + miny + sensorWidth + sensorsAngle)
				trianglePath.addNodeAtEnd(maxx,					offset + miny + sensorWidth)
				trianglePath.addNodeAtEnd(maxx,					offset + miny)
				trianglePath.addNodeAtEnd(minx + boundsWidth/2,	offset + miny + sensorsAngle)
				trianglePath.setIsClosed(True)
				
				offset += distance
				
				sensorPaths.append(trianglePath)
		
		else:
			distance = sensorWidth + padding
			offset = initialOffset
			
			if padding < sensorWidth/2 :
				offset -= distance
			
			sensorPaths = []
			
			while offset < boundsWidth :
				trianglePath = Path()
				trianglePath.addNodeAtEnd(offset + minx,								miny)
				trianglePath.addNodeAtEnd(offset + minx + sensorWidth,					miny)
				trianglePath.addNodeAtEnd(offset + minx + sensorWidth + sensorsAngle,	miny + boundsHeight/2)
				trianglePath.addNodeAtEnd(offset + minx + sensorWidth,					maxy)
				trianglePath.addNodeAtEnd(offset + minx,								maxy)
				trianglePath.addNodeAtEnd(offset + minx + sensorsAngle,					miny + boundsHeight/2)
				trianglePath.setIsClosed(True)
				
				offset += distance
				
				sensorPaths.append(trianglePath)
		
		# cut with polygon mask
		sensorPolys = [converter.pathToPolygon(path) for path in sensorPaths]

		pathMaskedSensors = []

		for poly in sensorPolys:
			polyMasked = poly.intersection(polyMask)

			pathMasked = converter.polygonsToPath([polyMasked])
			if isinstance(pathMasked, list):#multiple polygons created e.g. sensor cut into parts by mask
				pathMaskedSensors.extend(pathMasked)
			elif len(pathMasked) != 0: #one polygon, not empty
				pathMaskedSensors.append(pathMasked)
			
		for triangleMasked in pathMaskedSensors:
			style = {'stroke' : 'none', 'stroke-width': '1', 'fill': '#000000'}
			attribs = {'style': simplestyle.formatStyle(style)}
			svgRectPath = inkex.etree.Element(inkex.addNS('path','svg'),attribs)
			triangleMasked.toSVG(svgRectPath)
			layer.append(svgRectPath) 
		
		
		# x, y = y, x
		
		


effect = InsertSlider()
effect.affect()

