import random
import sys
import copy

from PIL import Image

imgx = 500; imgy = 500
image = Image.new("RGB", (imgx, imgy))
pixels = image.load()
mx = 50; my = 50 # width and height of the maze
maze = [[0 for x in range(mx)] for y in range(my)]
dx = [0, 1, 0, -1]; dy = [-1, 0, 1, 0] # directions to move in the maze

def GenerateMaze(cx, cy):
    maze[cy][cx] = 1
    while True:
        # find a new cell to add
        nlst = [] # list of available neighbors
        for i in range(4):
            nx = cx + dx[i]; ny = cy + dy[i]
            if nx >= 0 and nx < mx and ny >= 0 and ny < my:
                if maze[ny][nx] == 0:
                    # of occupied neighbors of the candidate cell must be 1
                    ctr = 0
                    for j in range(4):
                        ex = nx + dx[j]; ey = ny + dy[j]
                        if ex >= 0 and ex < mx and ey >= 0 and ey < my:
                            if maze[ey][ex] == 1: ctr += 1
                    if ctr == 1: nlst.append(i)
        # if 1 or more available neighbors then randomly select one and add
        if len(nlst) > 0:
            ir = nlst[random.randint(0, len(nlst) - 1)]
            cx += dx[ir]; cy += dy[ir]
            GenerateMaze(cx, cy)
        else: break



def solveMaze(binary_grid, x_src, y_src, x_dst,y_dst,startValue):

    if (binary_grid[y_src][x_src] or binary_grid[y_dst][x_dst])  == 0:
        print("Wrong start or destination point")
        print("StartValue= %d, Y_src=%d,X_src=%d,Y_dst=%d,X_dst=%d "%(startValue,y_src,x_src,y_dst,x_dst))
        return False

    pictureHeight = len(binary_grid)
    pictureWidth = len(binary_grid[0])
    edge = [(x_src,y_src)]
    binary_grid[y_src][x_src] = startValue
    value=startValue
    solvable=False
    while edge:
        newedge = []
        value+=1
        for (x, y) in edge:
            if (x==x_dst and y==y_dst):
                print(value)
                solvable=True
                break
            for (s, t) in ((x+1, y), (x-1, y), (x, y+1), (x, y-1)):
                if 0<=s<pictureWidth and  0<=t<pictureHeight and binary_grid[t][s]==1:
                    binary_grid[t][s] = value
                    newedge.append((s, t))
        edge = newedge
    if not solvable:
        print("Maze is not solvable")
        return False
    path=[(x_dst,y_dst)]
    while True:
        (x,y) =path[len(path)-1]
        if (x==x_src and y==y_src):
            break
        for (s, t) in ((x+1, y), (x-1, y), (x, y+1), (x, y-1)):
            if 0<=s<pictureWidth and  0<=t<pictureHeight and binary_grid[t][s]==binary_grid[y][x]-1:
                path.append((s,t))
    return path


GenerateMaze(0, 0)
# paint the maze
for ky in range(imgy):
    for kx in range(imgx):
        m = maze[my * ky // imgy][mx * kx // imgx] * 255
        pixels[kx, ky] = (m, m, m)
image.save("RandomMaze_" + str(mx) + "x" + str(my) + ".png", "PNG")
print("Maze Generated")
result=solveMaze(copy.deepcopy(maze),0,0,0,49,4)

# paint Maze result if solvable
if not result ==False:
    print("MazeSolved")
    result.reverse()
    print( result )
    for ky in range(imgy):
        for kx in range(imgx):
            in_x=mx * kx // imgx
            in_y=my * ky // imgy
            if (in_x,in_y) in result:
                pixels[kx, ky] = (255, 0, 0)
            else:
                m = maze[in_y][in_x] * 255
                pixels[kx, ky] = (m, m, m)
    image.save("RandomMazeSolution_" + str(mx) + "x" + str(my) + ".png", "PNG")

