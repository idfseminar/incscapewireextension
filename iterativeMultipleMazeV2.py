import random
import sys
import copy
import math
from random import randint
from inkex import debug


def solveMaze(binary_grid, x_src, y_src, x_dst, y_dst):
    start_value = 4

    # if (binary_grid[y_src][x_src] == 0) or (binary_grid[y_dst][x_dst] == 0):
    #    debug("Wrong start or destination point")
    #    debug("StartValue= %d, Y_src=%d,X_src=%d,Y_dst=%d,X_dst=%d ", start_value, y_src, x_src, y_dst, x_dst)
    #    return False

    picture_height = len(binary_grid[0])
    picture_width = len(binary_grid)
    edge = [(x_src, y_src)]
    binary_grid[x_src][y_src] = start_value
    value = start_value
    solvable = False
    while edge:
        newedge = []
        value += 1
        for (x, y) in edge:
            if (x == x_dst) and (y == y_dst):
                solvable = True
                break
            for (s, t) in ((x+1, y), (x-1, y), (x, y+1), (x, y-1)):
                if 0 <= s < picture_width and 0 <= t < picture_height:
                    if binary_grid[s][t] == 1 or (s == x_dst and t == y_dst):
                        binary_grid[s][t] = value
                        newedge.append((s, t))
        edge = newedge
    if not solvable:
        # print("Maze is not solvable")
        return False
    path = [(x_dst, y_dst)]
    while True:
        found_way = False
        (x, y) = path[-1]
        if (x == x_src) and (y == y_src):
            break

        for (s, t) in ((x-1, y), (x+1, y)):
            if 0 <= s < picture_width and 0 <= t < picture_height and binary_grid[s][t] <= binary_grid[x][y]-1 and binary_grid[s][t] != 0:
                path.append((s, t))
                found_way = True
                break

        if found_way is True:
            continue

        for (s, t) in ((x+1, y+1), (x-1, y+1)):
            if 0 <= s < picture_width and 0 <= t < picture_height and (binary_grid[s][t] <= binary_grid[x][y]-2 or binary_grid[s][t] == binary_grid[x][y]-1) and binary_grid[s][t] != 0:
                path.append((s, t))
                found_way = True
                break

        if found_way is True:
            continue

        for (s, t) in ((x+1, y-1), (x-1, y-1)):
            if 0 <= s < picture_width and 0 <= t < picture_height and (binary_grid[s][t] <= binary_grid[x][y]-2 or binary_grid[s][t] == binary_grid[x][y]-1) and binary_grid[s][t] != 0:
                path.append((s, t))
                found_way = True
                break

        if found_way is True:
            continue

        for (s, t) in ((x, y+1), (x, y-1)):
            if 0 <= s < picture_width and 0 <= t < picture_height and binary_grid[s][t] <= binary_grid[x][y]-1 and binary_grid[s][t] != 0:
                path.append((s, t))
                break

    return path
