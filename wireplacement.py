#!/usr/bin/env python
# Vikram Narayanan
# Magnus Halbe (use of shapely to generate obstacle arrays)

import sys
sys.path.append('/usr/share/inkscape/extensions')
import inkex
from inkex import debug
import simplestyle
import shapely
from shapely.geometry.linestring import LineString
import iterativeMultipleMazeV2
import a_star_maze
from printedelectronics.pathhelper import *
from printedelectronics.pathshapelyconverter import Converter
import math
import copy
from overlay_grid import *
from connectors import *
import operator

class WirePlacement(inkex.Effect):
    """
    Creates wire connectors for connecting all the sensor
    elements in a sensor (e.g., multitouch, ring-slider)
    """
    def __init__(self):
        # Call the base class constructor.
        inkex.Effect.__init__(self)
        self.OptionParser.add_option('--connector_position1', action = 'store',
                                     type='string', dest = 'conn_pos1', default = 'bottom',
                                     help = 'connector position')

        self.OptionParser.add_option('--connector_spacing', action = 'store',
                                     type = 'int', dest = 'conn_spacing', default = '10',
                                     help = 'Spacing between connectors')

        self.OptionParser.add_option('--wire_width', action = 'store',
                                     type = 'float', dest = 'wire_width', default = '2.48',
                                     help = 'Width of the drawn wires')

        self.OptionParser.add_option('--spacing', action = 'store',
                                     type = 'int', dest = 'spacing', default = '10',
                                     help = 'Spacing between all objects')

        self.OptionParser.add_option('--path_finding_detail', action = 'store',
                                     type = 'string', dest = 'path_finding_detail', default = 'medium',
                                     help = 'Path finding detail')

        self.OptionParser.add_option('--draw_wires', action = 'store',
                                     type = 'inkbool', dest = 'draw_wires', default = 'true',
                                     help = 'If the wires should be drawn directly')

        self.OptionParser.add_option('--algorithm', action = 'store',
                                     type = 'int', dest = 'used_algo', default = '1',
                                     help = 'Used algorithm for connecting')

        # Tab is only used for design purposes, but Inkscape sends this as another parameter
        self.OptionParser.add_option('--tab', action = 'store', type = 'string', dest = 'ignore')

    @staticmethod
    def draw_path(path_array, width, color, layer):
        """
        Draws a simple path given by an array with x,y coordinates
        :param path_array: point array which build ups the path
        :param width: line width
        :param color: line color
        :param layer: layer on which the path will be placed
        """
        line = Path()
        line.setNodes(path_array)
        style = {'stroke': color,
                 'stroke-width': width,
                 'fill': 'none'}
        attribs = {'style': simplestyle.formatStyle(style)}
        svg_line_path = inkex.etree.SubElement(layer, inkex.addNS('path', 'svg'), attribs)
        line.toSVG(svg_line_path)
        return line

    @staticmethod
    def sort_coord_list(sort_x, list, increasing):
        """
        Sorts a list filled with coordinates (x,y) by x (when sort_vertical == false) or by y otherwise
        :param sort_x: considering the x values when true. Otherwise the y values will be considered
        :param list: coordinate list that have to be sorted
        :param increasing: sorting from small to large if true
        :return: the sorted coord list
        """
        # result = sorted(list, key=getKey)
        if sort_x is True:
            result = sorted(list, key=operator.itemgetter(0, 1))
        else:
            result = sorted(list, key=operator.itemgetter(1, 0))

        if increasing is True:
            return result
        return WirePlacement.reverse_list(result)

    @staticmethod
    def split_list(size_1, list):
        list_1 = []

        if size_1 > len(list):
            return False

        helper_list = copy.deepcopy(list)

        while len(list_1) < size_1:
            element = helper_list.pop(0)
            list_1.append(element)

        list_2 = helper_list
        return list_1, list_2


    @staticmethod
    def reverse_list(list):
        """
        Simple method to revert a list
        :param list:
        :return: reverted list
        """
        rev_list = []
        for i in reversed(list):
            rev_list.append(i)
        return rev_list

    @staticmethod
    def list_divide_components(list, divisor):
        result = []
        for elem in list:
            result.append(elem / divisor)

        return result

    def lookup_sensors(self, svgdoc, obstacle_list, destination_list):
        """
        Looks for sensors in the SensorLayer of the given svgdoc, creates shapley objects out of the found objects
        and places them into the obstacle_list. Out of this the destination coordinates will be computed and placed
        into the destination_list
        :param svgdoc: svg with the layer 'SensorLayer'
        :param obstacle_list: list where found sensors will be placed as shapley objects
        :param destination_list: list where the destination coordinates of all sensors will be placed
        :returns the amount of found sensors
        """
        sensor_amount = 0
        # TODO: What to do if there is no sensorLayer?
        for child in svgdoc.findall('{http://www.w3.org/2000/svg}g'):
            if 'SensorLayer' in child.attrib.values():
                # Try to find all possible sensors, export them as shapely objects and note there conection positions
                for elem in child:
                    if Path.isValid(elem) is False:
                        return
                    else:
                        pathMask = Path(elem)

                        # From Path to Shapely Polygon
                        polyMask = Converter.pathToLinearRing(pathMask)
                        linear_ring_poly = polyMask.centroid
                        sensor_obstcale = (linear_ring_poly.x, linear_ring_poly.y, polyMask)

                        destination_list.append(sensor_obstcale)

                sensor_amount = len(child)

        return sensor_amount

    def configure_selected_sensors(self, destination_list):
        """
        Writes (x, y, polygon) triples of selected objects into the destination list
        :param destination_list:
        """
        sensor_count = 0

        for obj_path in self.selected.values():

            act_path = Path(obj_path)
            obj_poly = Converter.pathToLinearRing(act_path)
            obj_point = obj_poly.centroid

            sensor_obstcale = (obj_point.x, obj_point.y, obj_poly)
            destination_list.append(sensor_obstcale)

        return len(destination_list)

    def lookup_obstacles(self, svgdoc, obstacle_list, destination_list):
        for child in svgdoc.findall('{http://www.w3.org/2000/svg}g'):
            # Try to find all possible obstacles, export them as shapely
            for elem in child:
                if Path.isValid(elem) is False:
                    return
                else:
                    pathMask = Path(elem)
                    # From Path to Shapely Polygon
                    if 'WireLayer' in child.attrib.values():
                        poly_mask = Converter.pathToLineString(pathMask)
                    else:
                        poly_mask = Converter.pathToLinearRing(pathMask)

                    for poly in destination_list:
                        # Do not set the selected polygons in the obstacle list
                        if not poly_mask.equals(poly[2]):
                            obstacle_list.append(poly_mask)

    def connect_points(self, start_point, end_point, logic_grid):
        """
        Connects two (x, y) points in the given grid and also fills the path route in the grid with zeros.
        The line will be drawn on the wire layer
        :param start_point: start point (connoector)
        :param end_point: end point (sensor)
        :param logic_grid: grid, where sensors, wires and connectors are marked with 1
        :returns line_string: A shapely LineString object that represents the created wire
        """
        logic_grid_maze = copy.deepcopy(logic_grid)

        if self.options.used_algo == 1:
            found_path = iterativeMultipleMazeV2.solveMaze(logic_grid_maze, start_point[0], start_point[1], end_point[0], end_point[1])
        elif self.options.used_algo == 2:
            found_path = a_star_maze.solveMaze(logic_grid_maze, start_point[0], start_point[1], end_point[0], end_point[1])

        if found_path is not False:
            real_coord_path = []

            for point in found_path:
                logic_grid[point[0]][point[1]] = 0  # now occupied by path
                real_coord_path.append(((point[0]*self.cellWidth + self.cellWidth/2), (point[1]*self.cellHeight + self.cellHeight/2)))

            line_path = WirePlacement.draw_path(real_coord_path, self.options.wire_width, '#000000', self.wire_layer)

            return LineString(real_coord_path)
        return False

    # overridden function
    def effect(self):

        if len(self.selected) > 0:
            self.selection_mode = True
        else:
            self.selection_mode = False

        svgdoc = self.document.getroot()
        svgwidth = self.unittouu(svgdoc.get('width'))
        svgheight = self.unittouu(svgdoc.get('height'))

        '''
        * find all g elements in the svg tree
        * check if SensorLayer is present
        * get all the path elements in the SensorLayer
        * validate path
        * calculate number of sensor elements
        * get shapely versions and save them
        '''

        obstacle_list = []
        destination_list = []
        start_list = []

        if self.selection_mode is False:
            num_sensors = self.lookup_sensors(svgdoc, obstacle_list, destination_list)
        else:
            # wirting sensors in destination list
            num_sensors = self.configure_selected_sensors(destination_list)

            # setting all other objects as obstacles in obstacle list
            self.lookup_obstacles(svgdoc, obstacle_list, destination_list)

        if num_sensors == 0:
                inkex.errormsg('No sensor elements found to wire.')
                inkex.errormsg('Maybe the sensor objects are not in the layer "SensorLayer"')
                return

        # sort list of destination positions by x value and setting largest x value first
        help_copy = copy.deepcopy(destination_list)
        # help_copy = WirePlacement.sort_coord_list(False, help_copy, True)
        # up, below = WirePlacement.split_list(int(len(help_copy) / 2), help_copy)
        # below_copy = copy.deepcopy(below)
        # below = WirePlacement.reverse_list(below_copy)
        # help_copy = up + below
        destination_list = help_copy

        # create a new layer for connectors
        clayer = inkex.etree.SubElement(svgdoc, 'g')
        clayer.set(inkex.addNS('label', 'inkscape'), 'ConnectorLayer')
        clayer.set(inkex.addNS('groupmode', 'inkscape'), 'layer')

        # TODO: Respect existing connectors
        connectors = ConnectorSpace(self.options, svgwidth, svgheight, clayer, num_sensors)

        # if self.options.conn_type == 'conn_croc':
        connectors.add_croc_connector()
        # elif self.options.conn_type == 'conn_10x2':
        #     connectors.add_parallel_connector(20)
        # elif self.options.conn_type == 'conn_8x2':
        #     connectors.add_parallel_connector(16)

        if self.options.draw_wires is False:
            return

        # convert the new created connectors to shapley objects, add them the obstacles list
        # and also save the starting positions
        for elem in clayer:

            elem_path = Path(elem)
            # From Path to Shapely Polygon
            poly_conn = Converter.pathToLinearRing(elem_path)

            # Connection point is the shapely centroid
            connector_link = poly_conn.centroid
            connector_obstacle = (connector_link.x, connector_link.y, poly_conn)
            start_list.append(connector_obstacle)

        svg_width = self.unittouu(svgdoc.get('width'))
        svg_height = self.unittouu(svgdoc.get('height'))

        if self.options.path_finding_detail == "high":
            cell_detail_factor = 1
        elif self.options.path_finding_detail == "medium":
            cell_detail_factor = 5
        else:
            cell_detail_factor = 10

        grid = OverlayGrid(obstacle_list, start_list, destination_list, svg_width, svg_height, self.options.wire_width, self.options.spacing, cell_detail_factor)

        self.cellWidth = grid.cell_width_minimum
        self.cellHeight = grid.cell_height_minimum

        if len(start_list) < len(destination_list):
            inkex.errormsg('There are less connector pins than sensors. '
                           'Increase number of connectors or reduce number of sensors.')
            return

        if len(grid.dst_list) > len(grid.start_list):
            inkex.errormsg('Too few connector pins for all the sensors. '
                           'Increase number of pins. '
                           'DST:' + str(len(grid.dst_list)) + ' STRT: ' + str(len(grid.start_list)))
            return

        # create layer for the wires
        self.wire_layer = inkex.etree.SubElement(svgdoc, 'g')
        self.wire_layer.set(inkex.addNS('label', 'inkscape'), 'WireLayer')
        self.wire_layer.set(inkex.addNS('groupmode', 'inkscape'), 'layer')

        # Try to connect all start points to the referring destination points
        successes = 0
        for i in range(0, len(grid.dst_list)):

            start = grid.start_list[i]
            dest = grid.dst_list[i]

            start_point = (start[0], start[1])
            dst_point = (dest[0], dest[1])

            ready_grid = grid.get_grid_manipulate_obstacles_id(i, False)

            wire_line_string = self.connect_points(start_point, dst_point, ready_grid)
            grid.logical_grid = ready_grid
            grid.logical_grid = grid.get_grid_manipulate_obstacles_id(i, True)
            if wire_line_string != False:
                grid.add_wire(wire_line_string) #pass the wire to the shapely space
                successes += 1


wplacement = WirePlacement()
'''
This calls the affect() function from inkex.py which makes
all the changes to the document
'''
wplacement.affect()
