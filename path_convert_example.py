#!/usr/bin/env python

import inkex
from printedelectronics.pathhelper import Path
from printedelectronics.pathshapelyconverter import Converter

class PathConvertExampleEffect(inkex.Effect):
    """
    Example that demonstrates the usage of the Path to/from
    shapely Polygon converter.

    Select two (!) overlapping (!) paths of straight lines. The
    effect will compute the union of the paths and replace the
    second element with the union.

    @authors: Michael Hedderich & Lena Hegemann
    @date: 2015/12/14
    """
    def __init__(self):
        inkex.Effect.__init__(self)

    def effect(self):
        if not len(self.selected) == 2:
            inkex.errormsg("you must select exactly 2 elements")
            return
        
        converter = Converter()
        
        # Users might create invalid objects that the Path object can not handle correctly.
        if not (Path.isValid(self.selected.values()[0]) and Path.isValid(self.selected.values()[1])):
            inkex.errormsg("One of the selected objects is invalid and can not be handled by the Path object. A reason might be that it contains curved lines. Only straight lines are supported.")
            return
        
        # from Inkscape svg to Path
        path1 = Path(self.selected.values()[0])
        path2 = Path(self.selected.values()[1])
        # from Path to Shapely Polygon
        polygon1 = converter.pathToPolygon(path1)
        polygon2 = converter.pathToPolygon(path2)
        
        # do some fancy stuff with the Shapely library
        unionPoly = polygon1.union(polygon2)
        
        # from Polygon to Path
        unionPath = converter.polygonsToPath([unionPoly])[0] #(expects list of polygons and returns list of paths)
        # from Path to Inkscape svg (overrides path of both elements)
        unionPath.toSVG(self.selected.values()[1])
        unionPath.toSVG(self.selected.values()[0])

effect = PathConvertExampleEffect()
effect.affect()

