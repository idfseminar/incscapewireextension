from heapq import *
import numpy


def heuristic(a, b):
    return (b[0] - a[0]) ** 2 + (b[1] - a[1]) ** 2


def astar(array, start, goal):

    neighbors = [(0, 1), (0, -1), (1, 0), (-1, 0), (1, 1), (1, -1), (-1, 1), (-1, -1)]

    close_set = set()
    came_from = {}
    gscore = {start: 0}
    fscore = {start: heuristic(start, goal)}
    oheap = []

    heappush(oheap, (fscore[start], start))

    while oheap:

        current = heappop(oheap)[1]

        if current == goal:
            data = []
            while current in came_from:
                data.append(current)
                current = came_from[current]
            return data

        close_set.add(current)
        for i, j in neighbors:
            neighbor = current[0] + i, current[1] + j
            tentative_g_score = gscore[current] + heuristic(current, neighbor)
            if 0 <= neighbor[0] < array.shape[0]:
                if 0 <= neighbor[1] < array.shape[1]:
                    if array[neighbor[0]][neighbor[1]] == 1:
                        continue
                else:
                    # array bound y walls
                    continue
            else:
                # array bound x walls
                continue

            if neighbor in close_set and tentative_g_score >= gscore.get(neighbor, 0):
                continue

            if tentative_g_score < gscore.get(neighbor, 0) or neighbor not in [i[1]for i in oheap]:
                came_from[neighbor] = current
                gscore[neighbor] = tentative_g_score
                fscore[neighbor] = tentative_g_score + heuristic(neighbor, goal)
                heappush(oheap, (fscore[neighbor], neighbor))

    return False


def solveMaze(binary_grid, x_src, y_src, x_dst, y_dst):
    # for A star 1 is obstacle, takes coordinates as (y,x) y top to bottom, x left to right
    transposed = [[row[i] for row in binary_grid] for i in range(len(binary_grid[0]))]
    binary_grid = transposed
    for j in range(len(binary_grid)):  # y coordinate
        for i in range(len(binary_grid[0])):  # x coordinate
            if binary_grid[j][i] == 0:
                binary_grid[j][i] = 1
            else:
                binary_grid[j][i] = 0
    # convert array to numpy array for processing
    maze = numpy.array(binary_grid)

    # due to algorithm internals target and destination can't be obstacles.
    # Set points to 1, set to 0 after algorithm pass
    if maze[y_src][x_src] == 1:
        maze[y_src][x_src] = 0
    if maze[y_dst][x_dst] == 1:
        maze[y_dst][x_dst] = 0
    start = (y_src, x_src)
    destination = (y_dst, x_dst)

    result = astar(maze, start, destination)

    if maze[y_src][x_src] == 1:
        maze[y_src][x_src] = 0
    if maze[y_dst][x_dst] == 1:
        maze[y_dst][x_dst] = 0
    path = []

    if result is False:
        return False

    for point in result:
        path.append(tuple(reversed(point)))

    return path
